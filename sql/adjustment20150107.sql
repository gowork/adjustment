/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50628
Source Host           : localhost:3306
Source Database       : adjustment

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2016-01-07 00:30:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_college
-- ----------------------------
DROP TABLE IF EXISTS `t_college`;
CREATE TABLE `t_college` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '学院名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学院表';

-- ----------------------------
-- Records of t_college
-- ----------------------------

-- ----------------------------
-- Table structure for t_college_account
-- ----------------------------
DROP TABLE IF EXISTS `t_college_account`;
CREATE TABLE `t_college_account` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(16) DEFAULT NULL COMMENT '本地账户ID',
  `college_id` int(5) DEFAULT NULL COMMENT '学院ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学院账户表';

-- ----------------------------
-- Records of t_college_account
-- ----------------------------

-- ----------------------------
-- Table structure for t_direction
-- ----------------------------
DROP TABLE IF EXISTS `t_direction`;
CREATE TABLE `t_direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `major_id` int(11) DEFAULT NULL COMMENT '专业ID',
  `name` varchar(255) DEFAULT NULL COMMENT '方向名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专业方向表';

-- ----------------------------
-- Records of t_direction
-- ----------------------------

-- ----------------------------
-- Table structure for t_exam_info
-- ----------------------------
DROP TABLE IF EXISTS `t_exam_info`;
CREATE TABLE `t_exam_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '学生ID',
  `post_school_name` varchar(255) DEFAULT NULL COMMENT '考研报考学校',
  `post_school_major` varchar(255) DEFAULT NULL COMMENT '考研报考专业',
  `math_score` varchar(255) DEFAULT NULL COMMENT '数学分数',
  `english_score` varchar(255) DEFAULT NULL COMMENT '英语分数',
  `polity_score` varchar(255) DEFAULT NULL COMMENT '政治分数',
  `business1_score` varchar(255) DEFAULT NULL COMMENT '业务1分数',
  `business2_score` varchar(255) DEFAULT NULL COMMENT '业务2分数',
  `total_score` varchar(255) DEFAULT NULL COMMENT '总分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='考研信息表';

-- ----------------------------
-- Records of t_exam_info
-- ----------------------------

-- ----------------------------
-- Table structure for t_local_oauth
-- ----------------------------
DROP TABLE IF EXISTS `t_local_oauth`;
CREATE TABLE `t_local_oauth` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ticket_number` varchar(255) DEFAULT NULL COMMENT '准考证号',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `tel` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `idcard` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `role_id` int(3) DEFAULT NULL COMMENT '角色id',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='本地授权表';

-- ----------------------------
-- Records of t_local_oauth
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_login_log`;
CREATE TABLE `t_login_log` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '授权ID',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expires` bigint(10) DEFAULT NULL COMMENT '超时时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='登录记录表';

-- ----------------------------
-- Records of t_login_log
-- ----------------------------
INSERT INTO `t_login_log` VALUES ('1', '3', 'tja6h5', '1451274814');

-- ----------------------------
-- Table structure for t_major
-- ----------------------------
DROP TABLE IF EXISTS `t_major`;
CREATE TABLE `t_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `college_id` int(11) DEFAULT NULL COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '专业名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专业表';

-- ----------------------------
-- Records of t_major
-- ----------------------------

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(2) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '管理员', '1');
INSERT INTO `t_role` VALUES ('2', '学院', '1');
INSERT INTO `t_role` VALUES ('3', '教师', '1');
INSERT INTO `t_role` VALUES ('4', '教师（不可开账户）', '1');
INSERT INTO `t_role` VALUES ('5', '学生', '1');

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) NOT NULL COMMENT '本地授权ID',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) DEFAULT '男' COMMENT '性别',
  `emerg_contact_tel` varchar(20) DEFAULT NULL COMMENT '紧急联系电话',
  `undergraduate_school` varchar(255) DEFAULT NULL COMMENT '本科学校',
  `undergraduate_major` varchar(255) DEFAULT NULL COMMENT '本科专业',
  `is_accept_swap` int(1) DEFAULT NULL COMMENT '是否接受校内调剂',
  `evaluation` text COMMENT '自我评价',
  `special` text COMMENT '特长',
  `prize` text COMMENT '获奖情况',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生基本信息表';

-- ----------------------------
-- Records of t_student
-- ----------------------------

-- ----------------------------
-- Table structure for t_target
-- ----------------------------
DROP TABLE IF EXISTS `t_target`;
CREATE TABLE `t_target` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '本地授权ID',
  `target_college_id` int(11) DEFAULT NULL COMMENT '目标学院ID',
  `target_major_id` int(11) DEFAULT NULL COMMENT '目标专业ID',
  `target_direction_id` int(11) DEFAULT NULL COMMENT '目标方向ID',
  `target_level` int(2) DEFAULT NULL COMMENT '目标优先级   1：第一志愿    2：第二志愿',
  `all_log` text COMMENT '所有日志',
  `last_log` text COMMENT '最后一次日志',
  `status` int(2) DEFAULT NULL COMMENT '状态 1：新建    2：录取    3：不录取',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(10) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生目标表';

-- ----------------------------
-- Records of t_target
-- ----------------------------

-- ----------------------------
-- Table structure for t_teacher
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher`;
CREATE TABLE `t_teacher` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(16) DEFAULT NULL COMMENT '本地授权ID',
  `college_id` int(5) DEFAULT NULL COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '真实名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教师基本信息表';

-- ----------------------------
-- Records of t_teacher
-- ----------------------------
