/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50628
Source Host           : localhost:3306
Source Database       : adjustment

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2016-01-16 22:16:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_college
-- ----------------------------
DROP TABLE IF EXISTS `t_college`;
CREATE TABLE `t_college` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '学院名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='学院表';

-- ----------------------------
-- Records of t_college
-- ----------------------------
INSERT INTO `t_college` VALUES ('1', '	能源与机械工程学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('2', '电气工程学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('3', '计算机科学与技术学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('4', '经济与管理学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('5', '外国语学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('6', '成人教育学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('7', '环境与化学工程学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('8', '自动化工程学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('9', '电子与信息工程学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('10', '数理学院', '1', '1452691314');
INSERT INTO `t_college` VALUES ('11', '国际交流学院', '1', '1452691314');

-- ----------------------------
-- Table structure for t_college_account
-- ----------------------------
DROP TABLE IF EXISTS `t_college_account`;
CREATE TABLE `t_college_account` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(16) DEFAULT NULL COMMENT '本地账户ID',
  `college_id` int(5) DEFAULT NULL COMMENT '学院ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='学院账户表';

-- ----------------------------
-- Records of t_college_account
-- ----------------------------
INSERT INTO `t_college_account` VALUES ('1', '5', '1');

-- ----------------------------
-- Table structure for t_direction
-- ----------------------------
DROP TABLE IF EXISTS `t_direction`;
CREATE TABLE `t_direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `major_id` int(11) DEFAULT NULL COMMENT '专业ID',
  `name` varchar(255) DEFAULT NULL COMMENT '方向名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='专业方向表';

-- ----------------------------
-- Records of t_direction
-- ----------------------------
INSERT INTO `t_direction` VALUES ('1', null, '测试专业1', '1', '1452870479');
INSERT INTO `t_direction` VALUES ('2', null, '专业方向1', '1', '1452870616');
INSERT INTO `t_direction` VALUES ('3', '1', '专业方向1', '1', '1452870714');
INSERT INTO `t_direction` VALUES ('4', '1', '添加专业方向1', '1', '1452870745');
INSERT INTO `t_direction` VALUES ('5', '1', '添加专业方向2', '1', '1452870758');

-- ----------------------------
-- Table structure for t_exam_info
-- ----------------------------
DROP TABLE IF EXISTS `t_exam_info`;
CREATE TABLE `t_exam_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '学生ID',
  `post_school_name` varchar(255) DEFAULT NULL COMMENT '考研报考学校',
  `post_school_major` varchar(255) DEFAULT NULL COMMENT '考研报考专业',
  `math_score` varchar(255) DEFAULT NULL COMMENT '数学分数',
  `english_score` varchar(255) DEFAULT NULL COMMENT '英语分数',
  `polity_score` varchar(255) DEFAULT NULL COMMENT '政治分数',
  `business1_score` varchar(255) DEFAULT NULL COMMENT '业务1分数',
  `business2_score` varchar(255) DEFAULT NULL COMMENT '业务2分数',
  `total_score` varchar(255) DEFAULT NULL COMMENT '总分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='考研信息表';

-- ----------------------------
-- Records of t_exam_info
-- ----------------------------
INSERT INTO `t_exam_info` VALUES ('1', '4', '复旦大学', '金融学', '110', '56', '70', '120', null, '356');

-- ----------------------------
-- Table structure for t_local_oauth
-- ----------------------------
DROP TABLE IF EXISTS `t_local_oauth`;
CREATE TABLE `t_local_oauth` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ticket_number` varchar(255) DEFAULT NULL COMMENT '准考证号',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `tel` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `idcard` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `role_id` int(3) DEFAULT NULL COMMENT '角色id',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='本地授权表';

-- ----------------------------
-- Records of t_local_oauth
-- ----------------------------
INSERT INTO `t_local_oauth` VALUES ('3', '122', 'U122', '13917797065', 'sky@163.com', '511702197403222585', 'te9jottbwr9yyeryetybet9jr99sj79r', '5', '1', '1451149010');
INSERT INTO `t_local_oauth` VALUES ('4', 'user', 'Uuser', '13917797064', 'tom@163.com', '511702198002221308', 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '5', '1', '1452691314');
INSERT INTO `t_local_oauth` VALUES ('5', null, 'T101', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '2', '2', '1452691314');
INSERT INTO `t_local_oauth` VALUES ('6', null, 'T102', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '3', '1', '1452948330');
INSERT INTO `t_local_oauth` VALUES ('8', null, 'T103', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '3', '1', '1452949519');
INSERT INTO `t_local_oauth` VALUES ('15', null, 'T104', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '4', '1', '1452950807');
INSERT INTO `t_local_oauth` VALUES ('16', null, 'T105', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '4', '1', '1452951118');
INSERT INTO `t_local_oauth` VALUES ('17', null, 'admin', null, null, null, 'gwq7sfr9t9b7y97bbgyhgqyjoeqo88rg', '1', '1', '1452951118');

-- ----------------------------
-- Table structure for t_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_login_log`;
CREATE TABLE `t_login_log` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '授权ID',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expires` bigint(10) DEFAULT NULL COMMENT '超时时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='登录记录表';

-- ----------------------------
-- Records of t_login_log
-- ----------------------------
INSERT INTO `t_login_log` VALUES ('1', '3', 'tja6h5', '1451274814');
INSERT INTO `t_login_log` VALUES ('2', '4', '3uzz7p', '1452777714');
INSERT INTO `t_login_log` VALUES ('3', '4', '3kgdqf', '1452778021');
INSERT INTO `t_login_log` VALUES ('4', '4', 'po4exc', '1452778065');
INSERT INTO `t_login_log` VALUES ('5', '4', '4q9dww', '1452778196');
INSERT INTO `t_login_log` VALUES ('6', '5', '6n497h', '1452782747');
INSERT INTO `t_login_log` VALUES ('7', '4', '8wdo6g', '1452783351');
INSERT INTO `t_login_log` VALUES ('8', '4', '192ior', '1452783601');
INSERT INTO `t_login_log` VALUES ('9', '4', '419xgk', '1452784328');
INSERT INTO `t_login_log` VALUES ('10', '5', 'ivpl6y', '1452785392');
INSERT INTO `t_login_log` VALUES ('11', '5', '85j8z1', '1452785724');
INSERT INTO `t_login_log` VALUES ('12', '5', 'trv9n5', '1452787668');
INSERT INTO `t_login_log` VALUES ('13', '5', 'aweflr', '1452788248');
INSERT INTO `t_login_log` VALUES ('14', '4', 'bri73b', '1452863913');
INSERT INTO `t_login_log` VALUES ('15', '5', 'b7t2va', '1452863932');
INSERT INTO `t_login_log` VALUES ('16', '5', 'hd38xr', '1452955855');
INSERT INTO `t_login_log` VALUES ('17', '4', '7j2bww', '1452961178');
INSERT INTO `t_login_log` VALUES ('18', '5', '4yo6a1', '1452964987');
INSERT INTO `t_login_log` VALUES ('19', '4', '3leir6', '1453002005');
INSERT INTO `t_login_log` VALUES ('20', '5', 'e75uxq', '1453005425');
INSERT INTO `t_login_log` VALUES ('21', '5', 'novxw3', '1453007088');
INSERT INTO `t_login_log` VALUES ('22', '5', '2kj1w2', '1453007347');
INSERT INTO `t_login_log` VALUES ('23', '5', 'ao9l54', '1453007413');
INSERT INTO `t_login_log` VALUES ('24', '5', 'col34t', '1453007468');
INSERT INTO `t_login_log` VALUES ('25', '4', 'x7d2g5', '1453031261');
INSERT INTO `t_login_log` VALUES ('26', '5', 'r413hc', '1453032677');
INSERT INTO `t_login_log` VALUES ('27', '6', 'q1uocz', '1453034809');
INSERT INTO `t_login_log` VALUES ('28', '5', 'gns4m7', '1453035710');
INSERT INTO `t_login_log` VALUES ('29', '8', 'krm5oq', '1453035976');
INSERT INTO `t_login_log` VALUES ('30', '17', '7wgps3', '1453037598');

-- ----------------------------
-- Table structure for t_major
-- ----------------------------
DROP TABLE IF EXISTS `t_major`;
CREATE TABLE `t_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `college_id` int(11) DEFAULT NULL COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '专业名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='专业表';

-- ----------------------------
-- Records of t_major
-- ----------------------------
INSERT INTO `t_major` VALUES ('1', '1', '能源与动力工程（含卓越工程师班）', '1', '1452691314');
INSERT INTO `t_major` VALUES ('2', '1', '机械设计制造及其自动化', null, null);
INSERT INTO `t_major` VALUES ('3', '1', '机械电子工程', null, null);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(2) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '管理员', '1452691314');
INSERT INTO `t_role` VALUES ('2', '学院', '1452691314');
INSERT INTO `t_role` VALUES ('3', '教师', '1452691314');
INSERT INTO `t_role` VALUES ('4', '教师（不可开账户）', '1452691314');
INSERT INTO `t_role` VALUES ('5', '学生', '1452691314');

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) NOT NULL COMMENT '本地授权ID',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) DEFAULT '男' COMMENT '性别',
  `emerg_contact_tel` varchar(20) DEFAULT NULL COMMENT '紧急联系电话',
  `undergraduate_school` varchar(255) DEFAULT NULL COMMENT '本科学校',
  `undergraduate_major` varchar(255) DEFAULT NULL COMMENT '本科专业',
  `is_accept_swap` int(1) DEFAULT NULL COMMENT '是否接受校内调剂',
  `evaluation` text COMMENT '自我评价',
  `special` text COMMENT '特长',
  `prize` text COMMENT '获奖情况',
  `admission_status` int(2) DEFAULT NULL COMMENT '录取状态（null:还未录取，1:已录取）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='学生基本信息表';

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES ('1', '4', '张三', '男', '13546453532', '上海大学', '软件工程', '1', '自我评价1\n自我评价2', null, null, '1');

-- ----------------------------
-- Table structure for t_target
-- ----------------------------
DROP TABLE IF EXISTS `t_target`;
CREATE TABLE `t_target` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(10) DEFAULT NULL COMMENT '本地授权ID',
  `target_college_id` int(11) DEFAULT NULL COMMENT '目标学院ID',
  `target_major_id` int(11) DEFAULT NULL COMMENT '目标专业ID',
  `target_direction_id` int(11) DEFAULT NULL COMMENT '目标方向ID',
  `target_level` int(2) DEFAULT NULL COMMENT '目标优先级   1：第一志愿    2：第二志愿',
  `all_log` text COMMENT '所有日志',
  `last_log` text COMMENT '最后一次日志',
  `status` int(2) DEFAULT NULL COMMENT '状态 1：新建    2：录取    3：不录取',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  `modify_time` bigint(10) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='学生目标表';

-- ----------------------------
-- Records of t_target
-- ----------------------------
INSERT INTO `t_target` VALUES ('1', '4', '1', '1', '3', '1', '{\"applierId\":4,\"operateDate\":\"2016-01-16 19:35:22\",\"operateId\":5}', '{\"applierId\":4,\"operateDate\":\"2016-01-16 19:35:22\",\"operateId\":5}', '1', '1452877951', '1452944122');
INSERT INTO `t_target` VALUES ('2', '4', '1', '1', '4', '2', '{\"applierId\":4,\"operateDate\":\"2016-01-16 19:35:32\",\"operateId\":5}', '{\"applierId\":4,\"operateDate\":\"2016-01-16 19:35:32\",\"operateId\":5}', '1', '1452877951', '1452944132');

-- ----------------------------
-- Table structure for t_teacher
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher`;
CREATE TABLE `t_teacher` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `local_oauth_id` bigint(16) DEFAULT NULL COMMENT '本地授权ID',
  `college_id` int(5) DEFAULT NULL COMMENT '学院ID',
  `name` varchar(255) DEFAULT NULL COMMENT '真实名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='教师基本信息表';

-- ----------------------------
-- Records of t_teacher
-- ----------------------------
INSERT INTO `t_teacher` VALUES ('1', '6', '1', '李四');
INSERT INTO `t_teacher` VALUES ('2', '8', '1', '王五');
INSERT INTO `t_teacher` VALUES ('3', '10', '1', '李六');
INSERT INTO `t_teacher` VALUES ('4', '11', '1', '李六');
INSERT INTO `t_teacher` VALUES ('5', '12', '1', '李六');
INSERT INTO `t_teacher` VALUES ('6', '13', '1', '李六');
INSERT INTO `t_teacher` VALUES ('7', '14', '1', '李六');
INSERT INTO `t_teacher` VALUES ('8', '15', '1', '李六');
INSERT INTO `t_teacher` VALUES ('9', '16', '1', '李六1');
