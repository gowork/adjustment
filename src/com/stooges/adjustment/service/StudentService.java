package com.stooges.adjustment.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stooges.adjustment.entity.ExamInfo;
import com.stooges.adjustment.entity.Student;
import com.stooges.adjustment.entity.Target;
import com.stooges.adjustment.form.ApplyInfoForm;
import com.stooges.adjustment.form.BaseInfoForm;
import com.stooges.adjustment.form.ExamInfoForm;
import com.stooges.adjustment.form.TargetForm;
import com.stooges.adjustment.util.Constants;
import com.stooges.adjustment.util.SimpleValidator;
import com.stooges.adjustment.view.ApplyInfoView;
import com.stooges.adjustment.view.BaseInfoView;
import com.stooges.adjustment.view.ExamInfoView;

/**
 * 学生逻辑处理
 * @author 张超
 *
 */
@Service
public class StudentService {
	@Resource
	private SqlSession sqlSession;
	/**
	 * 根据本地授权ID查询学生基本信息
	 * @param localOauthId 本地授权ID
	 * @return 学生基本信息
	 */
	public BaseInfoView findBaseInfoByOauthId(Long localOauthId){
		return sqlSession.selectOne("studentDao.findBaseInfoByOauthId",localOauthId);
	}
	/**
	 * 根据本地授权ID查询学生成绩
	 * @param localOauthId 本地授权ID
	 * @return 学生成绩
	 */
	public ExamInfoView findExamInfoByOauthId(Long localOauthId){
		return sqlSession.selectOne("examInfoDao.findExamInfoByOauthId",localOauthId);
	}
	/**
	 * 根据本地授权ID查询学生申请调剂情况
	 * @param localOauthId
	 * @return
	 */
	public ApplyInfoView findApplyInfoByOauthId(Long localOauthId){
		return sqlSession.selectOne("targetDao.findApplyInfoByOauthId",localOauthId);
	}
	/**
	 * 保存学生基本信息
	 * @param baseInfoForm 学生基本信息表单
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean saveBaseInfo(BaseInfoForm baseInfoForm)throws Exception{
		//转换数据
		Student student = new Student();
		student.setLocalOauthId(baseInfoForm.getLocalOauthId());
		student.setName(baseInfoForm.getName());
		student.setSex(baseInfoForm.getSex());
		student.setEmergContactTel(baseInfoForm.getEmergContactTel());
		student.setUndergraduateSchool(baseInfoForm.getUndergraduateSchool());
		student.setUndergraduateMajor(baseInfoForm.getUndergraduateMajor());
		student.setEvaluation(baseInfoForm.getEvaluation());
		student.setSpecial(baseInfoForm.getSpecial());
		student.setPrize(baseInfoForm.getPrize());
		//更新数据
		boolean updateResult = sqlSession.update("studentDao.updateBaseInfo",student)>0;
		//更新失败后插入数据
		if(updateResult){
			return true;
		}else{
			return sqlSession.insert("studentDao.saveBaseInfo",student)>0;
		}
		
	}
	/**
	 * 保存考试信息
	 * @param examInfoForm 考试信息表单
	 * @return 保存结果
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean saveExamInfo(ExamInfoForm examInfoForm)throws Exception{
		//转换数据
		ExamInfo examInfo = new ExamInfo();
		examInfo.setLocalOauthId(examInfoForm.getLocalOauthId());
		examInfo.setPostSchoolName(examInfoForm.getPostSchoolName());
		examInfo.setPostSchoolMajor(examInfoForm.getPostSchoolMajor());
		if(SimpleValidator.isNotBlank(examInfoForm.getMathName())){
			examInfo.setMathName(examInfoForm.getMathName());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getMathScore())){
			examInfo.setMathScore(examInfoForm.getMathScore());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getEnglishName())){
			examInfo.setEnglishName(examInfoForm.getEnglishName());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getEnglishScore())){
			examInfo.setEnglishScore(examInfoForm.getEnglishScore());
		}
		examInfo.setPolityScore(examInfoForm.getPolityScore());
		if(SimpleValidator.isNotBlank(examInfoForm.getBusiness1Name())){
			examInfo.setBusiness1Name(examInfoForm.getBusiness1Name());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getBusiness1Score())){
			examInfo.setBusiness1Score(examInfoForm.getBusiness1Score());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getBusiness2Name())){
			examInfo.setBusiness2Name(examInfoForm.getBusiness2Name());
		}
		if(SimpleValidator.isNotBlank(examInfoForm.getBusiness2Score())){
			examInfo.setBusiness2Score(examInfoForm.getBusiness2Score());
		}
		examInfo.setTotalScore(examInfoForm.getTotalScore());
		//更新表单数据
		boolean updateResult = sqlSession.update("examInfoDao.updateExamInfo",examInfo)>0;
		//更新失败就插入数据
		if(updateResult){
			return true;
		}else{
			return sqlSession.insert("examInfoDao.saveExamInfo",examInfo)>0;
		}
	}
	/**
	 * 保存申请调剂信息
	 * @param applyInfoForm 申请调剂表单
	 * @return 保存结果
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean saveApplyInfo(ApplyInfoForm applyInfoForm)throws Exception{
		//转换数据
		Student student = new Student();
		student.setLocalOauthId(applyInfoForm.getLocalOauthId());
		student.setIsAcceptSwap(applyInfoForm.getIsAcceptSwap());

		List<TargetForm> targetFormList = applyInfoForm.getTargetFormList();
		List<Target> targetList = new ArrayList<Target>();
		for (TargetForm targetForm : targetFormList) {
			Target target = new Target();
			target.setLocalOauthId(applyInfoForm.getLocalOauthId());
			target.setStatus(Constants.TARGET_STATUS_CREATE);
			target.setModifyTime(System.currentTimeMillis()/1000);
			target.setTargetCollegeId(targetForm.getTargetCollegeId());
			target.setTargetMajorId(targetForm.getTargetMajorId());
			target.setTargetDirectionId(targetForm.getTargetDirectionId());
			target.setTargetLevel(targetForm.getTargetLevel());
			targetList.add(target);
		}
		//保存是否校内调剂
		Student dbStudent = sqlSession.selectOne("studentDao.findStudentByOauthId",applyInfoForm.getLocalOauthId());
		if(dbStudent!=null){
			sqlSession.update("studentDao.updStudent",student);
		}else{
			sqlSession.insert("studentDao.saveStudent",student);
		}
		
		//保存目标专业
		//查询数据库中的目标专业，与提交的数据进行比较，进行相应的更新和保存
		List<Target> dbTargetList  = sqlSession.selectList("targetDao.findTargetsByOauthId",applyInfoForm.getLocalOauthId());
		if(dbTargetList==null || dbTargetList.isEmpty()){
			for (Target target: targetList) {
				target.setCreateTime(System.currentTimeMillis()/1000);
			}
			return sqlSession.insert("targetDao.saveTargets",targetList)>0;
		}else{
			return sqlSession.update("targetDao.updateTargets",targetList)>0;
		}
	}
}
