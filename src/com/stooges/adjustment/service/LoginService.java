package com.stooges.adjustment.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.stooges.adjustment.entity.LocalOauth;
import com.stooges.adjustment.entity.LoginLog;
import com.stooges.adjustment.form.RegistForm;
import com.stooges.adjustment.util.CommonUtil;
import com.stooges.adjustment.util.Constants;
import com.stooges.adjustment.util.Md5Util;
import com.stooges.adjustment.view.OauthView;

/**
 * 登录注册处理
 * @author 张超
 *
 */
@Service("loginService")
public class LoginService {
	@Resource
	private SqlSession sqlSession;
	private static final Logger log = LoggerFactory.getLogger(LoginService.class);
	/**
	 * 根据输入的用户名  身份证号  准考证号来获得授权
	 * @param username 输入的用户名 身份证号 准考证号
	 * @return 授权信息
	 */
	public OauthView findOauthByInput(String username){
		try {
			return sqlSession.selectOne("localOauthDao.findOauthByInput",username);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
			return null;
		}
	}
	/**
	 * 根据cookies中保存的登录信息查询授权信息
	 * @param log cookies保存的登录日志
	 * @return 数据库中的授权信息
	 */
	public OauthView findOauthByLoged(LoginLog loginLog){
		try {
			return sqlSession.selectOne("localOauthDao.findOauthByLoged",loginLog);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
			return null;
		}
	}
	/**
	 * 保存登录日志
	 * @param loginLog 登录日志
	 */
	public void saveLoginLog(LoginLog loginLog){
		String token = CommonUtil.generate(6);
		loginLog.setToken(token);
		try {
			sqlSession.insert("loginLogDao.saveLoginLog",loginLog);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
		}
	}
	/**
	 * 验证注册表单正确性
	 * @param registForm 注册
	 * @param errorMap
	 * @return true：重复   false:不重复
	 */
	public boolean validateDuplicate(RegistForm registForm,Map<String, String> errorMap){
		try {
			String error = "";
			List<LocalOauth> list = sqlSession.selectList("localOauthDao.findRegistDuplicate", registForm);
			if(list!=null&&!list.isEmpty()&&list.size()>0){
				StringBuffer errorBuffer = new StringBuffer();
				for (LocalOauth localOauth : list) {
					if(registForm.getEmail().equals(localOauth.getEmail())){
						errorBuffer.append("邮箱 ");
					}
					if(registForm.getTel().equals(localOauth.getTel())){
						errorBuffer.append("手机 ");
					}
					if(registForm.getIdcard().equals(localOauth.getIdcard())){
						errorBuffer.append("身份证号 ");
					}
					if(registForm.getTicketNumber().equals(localOauth.getTicketNumber())){
						errorBuffer.append("准考证号 ");
					}
					if(errorBuffer.length()>0){
						error = errorBuffer.toString()+"已经被注册";
						errorMap.put("error", error);
						return true;
					}
				}
				error = "帐号已经被注册";
				errorMap.put("error", error);
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
			String error = "数据连接失败，请稍候重试";
			errorMap.put("error",error);
			return true;
		}
	}
	/**
	 * 注册
	 * @param registForm 注册表单
	 * @return
	 */
	public boolean regist(RegistForm registForm){
		try {
			LocalOauth la = new LocalOauth();
			la.setEmail(registForm.getEmail());
			la.setIdcard(registForm.getIdcard());
			la.setTel(registForm.getTel());
			la.setTicketNumber(registForm.getTicketNumber());
			la.setUsername("U"+registForm.getTicketNumber());
			la.setPassword(Md5Util.MD5(registForm.getPassword()));
			la.setRoleId(Constants.ROLE_STUDENT);
			la.setStatus(Constants.LOCAL_OAUTH_STATUS_ACTIVATED);
			la.setCreateTime(System.currentTimeMillis()/1000);
			return sqlSession.insert("localOauthDao.saveLocalOauth",la)>0;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
			return false;
		}
	}

}
