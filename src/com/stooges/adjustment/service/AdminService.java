package com.stooges.adjustment.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stooges.adjustment.entity.LocalOauth;
import com.stooges.adjustment.form.AccountSearchForm;
import com.stooges.adjustment.util.Md5Util;
import com.stooges.adjustment.view.AccountView;

/**
 * 管理员逻辑处理
 * @author 张超
 *
 */
@Service
public class AdminService {
	@Resource
	private SqlSession sqlSession;
	/**
	 * 查询所有账户
	 * @param accountSearchForm
	 * @return
	 */
	public PageInfo<AccountView> findAccoutList(AccountSearchForm accountSearchForm) {
		int pageNum = 1;
		int pageSize = 10;
		if(accountSearchForm!=null){
			if(accountSearchForm.getPageNum()!=null){
				pageNum = accountSearchForm.getPageNum();
			}
			if(accountSearchForm.getPageSize()!=null){
				pageSize = accountSearchForm.getPageSize();
			}
		}
		PageHelper.startPage(pageNum, pageSize);
		List<AccountView> accountList = sqlSession.selectList("localOauthDao.findAccoutList",accountSearchForm);
		return new PageInfo<AccountView>(accountList);
	}
	/**
	 * 停用帐号
	 * @param localOauthId 本地授权id
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean deadAccount(Long localOauthId)throws Exception{
		return sqlSession.update("localOauthDao.deadAccount",localOauthId)>0;
	}
	/**
	 * 重置密码
	 * @param localOauthId 本地授权ID
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean resetPassword(Long localOauthId)throws Exception {
		LocalOauth localOauth = new LocalOauth();
		localOauth.setId(localOauthId);
		localOauth.setPassword(Md5Util.MD5("123456"));
		return sqlSession.update("localOauthDao.changePassword",localOauth)>0;
	}

}
