package com.stooges.adjustment.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stooges.adjustment.entity.LocalOauth;
import com.stooges.adjustment.view.CollegeView;
import com.stooges.adjustment.view.DirectionView;
import com.stooges.adjustment.view.MajorView;

/**
 * 共通服务逻辑处理
 * @author 张超
 *
 */
@Service
public class CommonService {
	private static final Logger log = LoggerFactory.getLogger(CommonService.class);
	@Resource
	private SqlSession sqlSession;
	/**
	 * 更改密码
	 * @param localOauth 
	 * @return
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean changePassword(LocalOauth localOauth)throws Exception{
		if(localOauth==null){
			return false;
		}else{
			try {
				return sqlSession.update("localOauthDao.changePassword",localOauth)>0;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return false;
			}
		}
	}
	/**
	 * 查询所有学院
	 * @return
	 */
	public List<CollegeView> findAllColleges(){
		return sqlSession.selectList("collegeDao.findAllColleges");
	}
	/**
	 * 根据本地授权ID查询学院ID
	 * @param roleId 角色ID
	 * @param oauthId 本地授权ID
	 * @return
	 */
	public Integer findCollegeIdByOauthId(Integer roleId,Long oauthId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("roleId", roleId);
		map.put("oauthId", oauthId);
		return sqlSession.selectOne("collegeDao.findCollegeIdByOauthId",map);
	}
	/**
	 * 根据学院ID查询所有专业
	 * @param collegeId 学院ID
	 * @return
	 */
	public List<MajorView> findAllMajorByCollegeId(Integer collegeId) {
		return sqlSession.selectList("majorDao.findAllMajorByCollegeId",collegeId);
	}
	/**
	 * 根据专业ID查询所有方向
	 * @param majorId 专业ID
	 * @return
	 */
	public List<DirectionView> findAllDirectByMajorId(Integer majorId) {
		return sqlSession.selectList("directionDao.findAllDirectByMajorId",majorId);
	}
}
