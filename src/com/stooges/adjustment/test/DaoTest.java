package com.stooges.adjustment.test;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.stooges.adjustment.entity.Target;
import com.stooges.adjustment.view.OauthView;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml") //加载配置
public class DaoTest {
	@Resource
	private SqlSession sqlSession;
//	@Test
	public void testgetOauthByInput(){
		OauthView oauthView = sqlSession.selectOne("localOauthDao.findOauthByInput","u122");
		System.out.println(oauthView!=null);
	}
	@Test
	public void testUpdTargets(){
		List<Target> targetList = new ArrayList<Target>();
		Target target = new Target();
//		target.setLocalOauthId(applyInfoForm.getLocalOauthId());
//		target.setStatus(Constants.TARGET_STATUS_CREATE);
//		target.setModifyTime(System.currentTimeMillis()/1000);
//		target.setTargetCollegeId(targetForm.getTargetCollegeId());
//		target.setTargetMajorId(targetForm.getTargetMajorId());
//		target.setTargetDirectionId(targetForm.getTargetDirectionId());
//		target.setTargetLevel(targetForm.getTargetLevel());
		targetList.add(target);
		boolean updResult = sqlSession.update("targetDao.findOauthByInput",targetList)>0;
		System.out.println(updResult);
	}
}
