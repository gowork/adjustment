package com.stooges.adjustment.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.stooges.adjustment.service.LoginService;
import com.stooges.adjustment.view.OauthView;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml") //加载配置
public class ServiceTest {
	@Resource
	private LoginService loginService;
	@Test
	public void testgetOauthByInput(){
		OauthView oauthView = loginService.findOauthByInput("u122");
		System.out.println(oauthView!=null);
	}
}
