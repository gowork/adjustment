package com.stooges.adjustment.util;

import java.security.MessageDigest;


/**
 * 生成MD5值工具类
 *
 */

public class Md5Util {

	private static final char hexDigits[] = { 'q', 'w', 'e', 'r', 't', 'y', 'h', 'j', '8', '9', '7', 'b', 'f', 's', 'g', 'o' };

	/**
	 * 字符串MD5加密
	 * 
	 * @param s
	 * @return
	 */
	public static final String MD5(String s) {
		try {
			byte[] btInput = s.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			return byteToHexString(md);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 把byte[]数组转换成十六进制字符串表示形式
	 * 
	 * @param tmp要转换的数组
	 * @return 十六进制字符串表示形式
	 */
	public static String byteToHexString(byte[] tmp) {
		String s;
		char str[] = new char[16 * 2];
		int k = 0;
		for (int i = 0; i < 16; i++) {
			byte byte0 = tmp[i];
			str[k++] = hexDigits[byte0 >>> 4 & 0xf];
			str[k++] = hexDigits[byte0 & 0xf];
		}
		s = new String(str);
		return s;
	}
	public static void main(String[] args) {
		System.out.println(Md5Util.MD5("123456"));
	}
}
