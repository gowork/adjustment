package com.stooges.adjustment.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * JSON的操作工具类
 * @author 张超
 *
 */
public class JsonUtil {
	
	/**
	 * 将对象转换成JSON字符串
	 * @param obj
	 * @return
	 */
	public static String objToJson(Object obj) {
		JSONObject jsonObject = JSONObject.fromObject(obj);
		return jsonObject.toString();
	}

	/**
	 * 将List转换成JSON字符串
	 * @param <T>
	 * @param list
	 * @return
	 */
	public static <T> String listToJson(List<T> list) {
		JSONArray jsonArray = JSONArray.fromObject(list);
		return jsonArray.toString();
	}
	
	/**
	 * 将HashMap对象转成json格式
	 * @param obj
	 * @return
	 */
	public static String mapToJson(Map<?, ?> map) {
		JSONObject jsonObj = JSONObject.fromMap(map);
		return jsonObj.toString();
	}
	
	/**
	 * 将JSON转换成Map
	 * @param json
	 * @return
	 */
	public static Map<String, Object> jsonToMap(String jsonString){
		JSONObject jsonObject = JSONObject.fromString(jsonString);
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
        @SuppressWarnings("unchecked")
		Iterator<Object> it = jsonObject.keys();
        while (it.hasNext()){
            String key = String.valueOf(it.next());
            Object value = jsonObject.get(key);
            mapResult.put(key, value);
        }
        return mapResult;
	}
	/**
	 * 将JSON转换成String类型Map
	 * @param json
	 * @return
	 */
	public static Map<String, String> jsonToStringMap(String jsonString){
		JSONObject jsonObject = JSONObject.fromString(jsonString);
		HashMap<String, String> mapResult = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
		Iterator<String> it = jsonObject.keys();
		while (it.hasNext()){
			String key = String.valueOf(it.next());
			String value = (String) jsonObject.get(key);
			mapResult.put(key, value);
		}
		return mapResult;
	}
	/**
	 * 将JSON转换成Map
	 * @param json
	 * @return
	 */
	public static Map<String, Integer> jsonToIntegerMap(String jsonString){
		JSONObject jsonObject = JSONObject.fromString(jsonString);
		HashMap<String, Integer> mapResult = new HashMap<String, Integer>();
        @SuppressWarnings("unchecked")
		Iterator<String> it = jsonObject.keys();
        while (it.hasNext()){
        	String key = String.valueOf(it.next());
            Integer value = (Integer) jsonObject.get(key);
            mapResult.put(key, value);
        }
        return mapResult;
	}
	/**
	 * 将一个JSON字符串转换成实体对象
	 * @param jsonString
	 * @param classes
	 * @return
	 */
	public static Object jsonToObject(String jsonString, Class<?> classes) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		Object object = JSONObject.toBean(jsonObject, classes);
		return object;
	}
	
	/**
	 * 将对象转换为传入类型的List
	 * 
	 * @param <T>
	 * @param jsonArray
	 * @param objectClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> jsonToList(String jsonString, Class<T> objectClass) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		return JSONArray.toList(jsonArray, objectClass);
	}
	
	/**
	 * 将一个对象转换成JSON对象
	 * @param object
	 * @return
	 */
	public static JSONObject toJSONObject(Object object) {
		return JSONObject.fromObject(object);
	}
}
