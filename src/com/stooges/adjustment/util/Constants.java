package com.stooges.adjustment.util;

public class Constants {
	public static final String SESSION_USER = "user"; //session中的登录用户
	
	public static final String COOKIE_UID = "uid";    //登录日志的uid
	public static final String COOKIE_TOKEN = "token";//登录日志的token
	
	public static final Integer ROLE_ADMIN = 1;     //管理员
	public static final Integer ROLE_COLLEGE = 2;   //学院
	public static final Integer ROLE_TEACHER1 = 3;  //教师1 可以开账户
	public static final Integer ROLE_TEACHER2 = 4;  //教师2
	public static final Integer ROLE_STUDENT = 5;   //学生
	
	public static final Integer ROLE_STATUS_ACTIVATED = 1;  //在用状态
	public static final Integer ROLE_STATUS_DEAD = 2;       //停用状态
	
	public static final Integer LOCAL_OAUTH_STATUS_ACTIVATED = 1; //激活状态
	public static final Integer LOCAL_OAUTH_STATUS_DEAD = 2;      //停用状态
	
	public static final Integer ACCOUNT_TYPE_OPEN = 1;   //帐号类型为可以开新账户
	public static final Integer ACCOUNT_TYPE_DEFALUT = 2;//帐号类型为不可开新账户
	
	public static final Integer TARGET_STATUS_CREATE = 1;//新建
	public static final Integer TARGET_STATUS_ENROLL = 2;//录取
	public static final Integer TARGET_STATUS_REJECT = 3;//不录取

	public static final Integer MAJOR_STATUS_ACTIVATED = 1; //专业激活状态
	public static final Integer MAJOR_STATUS_DEAD = 2;      //专业停用状态
	public static final Integer DIRECT_STATUS_ACTIVATED = 1; //方向激活状态
	public static final Integer DIRECT_STATUS_DEAD = 2;      //方向停用状态
	
	public static final Integer STUDENT_STATUS_ENROLL = 1;//学生被录取
}
