package com.stooges.adjustment.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * java验证规则
 * 
 * @author 张超
 *
 */
public class SimpleValidator {
	/**
	 * 判断字符串是否不为空白 空白标准： 1、null 2、"" 3、去除开头和结尾的空白后为空或者null
	 * 
	 * @param input 输入的字符串
	 * @return 是否不为空
	 */
	public static boolean isNotBlank(String input) {
		return input != null && !("".equals(input.trim())) && !("null".equals(input.trim().toLowerCase()));
	}

	/**
	 * 判断字符串是否为空白
	 * 
	 * @param input 输入的字符串
	 * @return 是否为空
	 */
	public static boolean isBlank(String input) {
		return !isNotBlank(input);
	}

	/**
	 * 判断是否为正确的身份证
	 * 
	 * @param idcard 身份证
	 * @return
	 */
	public static boolean isValidatedIdcard(String idcard) {
		IdcardValidator idcardValidator = new IdcardValidator();
		return idcardValidator.isValidatedAllIdcard(idcard);
	}

	/**
	 * 判断是否为手机
	 * 
	 * @param mobile 手机号
	 * @return
	 */
	public static boolean isMobile(String mobile){
		return isMatcher("^((\\+?86)|(\\(\\+86\\)))?(13[012356789][0-9]{8}|15[012356789][0-9]{8}|17[012356789][0-9]{8}|18[02356789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$",mobile);
	}
	/**
	 * 判断是否为邮箱
	 * 
	 * @param email 邮箱
	 * @return 
	 */
	public static boolean isEmail(String email){
		return isMatcher("^([a-zA-Z0-9]+[_|\\_|\\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\\_|\\.]?)*[a-zA-Z0-9]+\\.[a-zA-Z]{2,3}$",email);
	}
	/**
	 * 判断是否符合正则表达式
	 * 
	 * @param pattern 正则表达式
	 * @param input 需要验证的字符 
	 * @return 
	 */
	public static boolean isMatcher(String pattern,String input){
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(input);
		return m.matches();
	}
}
