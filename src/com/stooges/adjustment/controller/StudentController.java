package com.stooges.adjustment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stooges.adjustment.form.ApplyInfoForm;
import com.stooges.adjustment.form.BaseInfoForm;
import com.stooges.adjustment.form.ExamInfoForm;
import com.stooges.adjustment.service.CommonService;
import com.stooges.adjustment.service.StudentService;
import com.stooges.adjustment.util.Constants;
import com.stooges.adjustment.view.ApplyInfoView;
import com.stooges.adjustment.view.BaseInfoView;
import com.stooges.adjustment.view.CollegeView;
import com.stooges.adjustment.view.ExamInfoView;
import com.stooges.adjustment.view.OauthView;
import com.stooges.adjustment.view.ResultView;

/**
 * 学生控制器
 * @author 张超
 *
 */
@RequestMapping("/student")
@Controller
public class StudentController {
	private static final Logger log = LoggerFactory.getLogger(StudentController.class);
	@Resource
	private StudentService studentService;
	@Resource
	private CommonService commonService;
	/**
	 * 跳转到保存基本信息页面
	 * @param session
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/baseinfo")
	public String toSaveBaseInfo(HttpSession session,ModelMap modelMap){
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		//查询基本信息
		BaseInfoView baseInfoView = studentService.findBaseInfoByOauthId(oauthView.getId());
		modelMap.put("baseInfo", baseInfoView);
		return "student/base-info";
	}
	/**
	 * 保存基本信息
	 * @param baseForm 基本信息
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/savebaseinfo",method = RequestMethod.POST)
	@ResponseBody
	public ResultView saveBaseInfo(@RequestBody BaseInfoForm baseForm,HttpSession session){
		ResultView resultView = new ResultView();
		//检查填写的表单是否正确
		String error = "";
		boolean validateResult = validateBaseInfo(baseForm,error);
		if(!validateResult){
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg(error);
			return resultView;
		}
		//将表单数据保存到数据库中
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		baseForm.setLocalOauthId(oauthView.getId());
		try {
			boolean saveResult = studentService.saveBaseInfo(baseForm);
			if(saveResult){
				resultView.setStatus(ResultView.SUCCESS);
			}else{
				resultView.setStatus(ResultView.ERROR);
				resultView.setMsg("保存失败，请稍候重试！");
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg("保存失败，请稍候重试！");
		}
		return resultView;
	}
	/**
	 * 跳转到保存考试成绩
	 * @param session
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/examinfo")
	public String toSaveExamInfo(HttpSession session,ModelMap modelMap){
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		//查询考试成绩
		ExamInfoView examInfoView = studentService.findExamInfoByOauthId(oauthView.getId());
		modelMap.put("examInfoView", examInfoView);
		return "student/exam-info";
	}
	
	/**
	 * 保存考试基本信息
	 * @param examInfoForm
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/saveexaminfo",method = RequestMethod.POST)
	@ResponseBody
	public ResultView saveExamInfo(@RequestBody ExamInfoForm examInfoForm,HttpSession session){
		ResultView resultView = new ResultView();
		//检查输入表单是否正确
		Map<String,String> errorMap = new HashMap<String,String>();
		boolean validateResult = validateExamInfo(examInfoForm,errorMap);
		if(!validateResult){
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg(errorMap.get("error"));
			return resultView;
		}
		
		//将表单保存到数据库中
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		examInfoForm.setLocalOauthId(oauthView.getId());
		try {
			boolean saveResult = studentService.saveExamInfo(examInfoForm);
			if(saveResult){
				resultView.setStatus(ResultView.SUCCESS);
			}else{
				resultView.setStatus(ResultView.ERROR);
				resultView.setMsg("保存失败，请稍候重试！");
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg("保存失败，请稍候重试！");
		}
		return resultView;
	}
	/**
	 * 跳转到保存调剂信息
	 * @param session
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/applyinfo")
	public String toSaveApplyInfo(HttpSession session,ModelMap modelMap){
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		//查询调剂信息
		ApplyInfoView applyInfoView = studentService.findApplyInfoByOauthId(oauthView.getId());
		modelMap.put("applyInfoView", applyInfoView);
		//查询全部学院
		List<CollegeView> collegeList = commonService.findAllColleges();
		modelMap.put("collegeList", collegeList);
		return "student/apply-info";
	}
	/**
	 * 保存调剂信息
	 * @param applyInfoForm
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/saveapplyinfo",method = RequestMethod.POST)
	@ResponseBody
	public ResultView saveApplyInfo(@RequestBody ApplyInfoForm applyInfoForm,HttpSession session){
		ResultView resultView = new ResultView();
		//检查输入表单是否正确
		Map<String,String> errorMap = new HashMap<String,String>();
		boolean validateResult = validateApplyInfo(applyInfoForm,errorMap);
		if(!validateResult){
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg(errorMap.get("error"));
			return resultView;
		}
		
		//将表单保存到数据库中
		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		applyInfoForm.setLocalOauthId(oauthView.getId());
		try {
			boolean saveResult = studentService.saveApplyInfo(applyInfoForm);
			if(saveResult){
				resultView.setStatus(ResultView.SUCCESS);
			}else{
				resultView.setStatus(ResultView.ERROR);
				resultView.setMsg("保存失败，请稍候重试！");
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg("保存失败，请稍候重试！");
		}
		return resultView;
	}
	/**
	 * 检查基本信息
	 * @param baseForm
	 * @param error
	 * @return
	 */
	private boolean validateBaseInfo(BaseInfoForm baseForm, String error) {
		//TODO
		return true;
	}
	/**
	 * 检查考试成绩基本信息是否正确
	 * @param examInfoForm 考试成绩等信息
	 * @param errorMap 错误
	 * @return
	 */
	private boolean validateExamInfo(ExamInfoForm examInfoForm, Map<String,String> errorMap) {
		//TODO
		return true;
	}
	/**
	 * 检查调剂信息表单是否正确
	 * @param applyInfoForm 调剂信息表单
	 * @param errorMap 错误
	 * @return
	 */
	private boolean validateApplyInfo(ApplyInfoForm applyInfoForm, Map<String,String> errorMap) {
		// TODO Auto-generated method stub
		return true;
	}

}
