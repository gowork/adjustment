package com.stooges.adjustment.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stooges.adjustment.entity.LocalOauth;
import com.stooges.adjustment.form.PasswordForm;
import com.stooges.adjustment.service.CommonService;
import com.stooges.adjustment.util.Constants;
import com.stooges.adjustment.util.Md5Util;
import com.stooges.adjustment.util.SimpleValidator;
import com.stooges.adjustment.view.DirectionView;
import com.stooges.adjustment.view.MajorView;
import com.stooges.adjustment.view.OauthView;
import com.stooges.adjustment.view.ResultView;

/**
 * 共通的控制器
 * @author 张超
 *
 */
@Controller
public class CommonController {
	private static final Logger log = LoggerFactory.getLogger(CommonController.class);
	@Resource
	private CommonService commonService;
	/**
	 * 
	 * @param majorCode
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/major/{majorCode}")
	public String showMajor(@PathVariable String majorCode,ModelMap modelMap){
		modelMap.put("majorCode", majorCode);
		return "major-introll";
	}
	/**
	 * 去更改密码
	 * @param session
	 * @return
	 */
	@RequestMapping("/tochangepassword")
	public String toChangePassword(HttpSession session){
		String url = "";
		OauthView oauth = (OauthView)session.getAttribute(Constants.SESSION_USER);
		int roleId = oauth.getRole().getId();
		switch(roleId){
			case 1:
				url = "admin/change-password";
				break;
			case 2:
				url = "college/change-password";
				break;
			case 3:
				url = "teacher/change-password";
				break;
			case 4:
				url = "teacher/change-password";
				break;
			case 5:
				url = "student/change-password";
				break;
			default:
				url = "error/404";
		}
		return url;
	}
	/**
	 * 更改密码
	 * @param passwordForm 密码表单
	 * @param session
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/changepassword")
	public String changePassword(PasswordForm passwordForm,HttpSession session,ModelMap modelMap){
		OauthView oauth = (OauthView)session.getAttribute(Constants.SESSION_USER);
		int roleId = oauth.getRole().getId();
		String pre = "";
		switch(roleId){
			case 1:
				pre = "admin/";
				break;
			case 2:
				pre = "college/";
				break;
			case 3:
				pre = "teacher/";
				break;
			case 4:
				pre = "teacher/";
				break;
			case 5:
				pre = "student/";
				break;
		}
		if(passwordForm==null){
			modelMap.put("passwordForm", new PasswordForm());
		}else{
			modelMap.put("passwordForm", passwordForm);
		}
		//检查输入的正确性
		if(passwordForm==null||
				SimpleValidator.isBlank(passwordForm.getOldPassword())||
				SimpleValidator.isBlank(passwordForm.getNewPassword())){
			modelMap.put("error", "请输入密码");
			return pre+"change-password";
		}else if(!Md5Util.MD5(passwordForm.getOldPassword()).equals(oauth.getPassword())){
			modelMap.put("error", "请输入正确的原始密码");
			return pre+"change-password";
		}
		//如果正确将数据更新到数据库中
		else{
			LocalOauth localOauth = new LocalOauth();
			localOauth.setId(oauth.getId());
			localOauth.setPassword(Md5Util.MD5(passwordForm.getNewPassword()));
			try {
				boolean changeResult = commonService.changePassword(localOauth);
				if(changeResult){
					return pre+"change-result";
				}else{
					modelMap.put("error", "系统繁忙，请稍候重试");
					return pre+"change-password";
				}
			} catch (Exception e) {
				log.error(e.getMessage(),e);
				modelMap.put("error", "系统繁忙，请稍候重试");
				return pre+"change-password";
			}
		}
	}
	/**
	 * 根据学院ID查询所有专业
	 * @param collegeId 学院ID
	 * @return
	 */
	@RequestMapping(value="/findAllMajorByCollegeId",method = RequestMethod.POST)
	@ResponseBody
	public ResultView findAllMajorByCollegeId(@RequestBody Integer collegeId){
		ResultView resultView = new ResultView(ResultView.SUCCESS);
		if(collegeId!=null){
			List<MajorView> majorList = commonService.findAllMajorByCollegeId(collegeId);
			if(majorList!=null&&!majorList.isEmpty()){
				resultView.setValue(majorList);
				return resultView;
			}
		}
		return resultView;
	}
	/**
	 * 根据专业ID查询所有方向
	 * @param majorId 专业ID
	 * @return
	 */
	@RequestMapping(value="/findAllDirectByMajorId",method = RequestMethod.POST)
	@ResponseBody
	public ResultView findAllDirectByMajorId(@RequestBody Integer majorId){
		ResultView resultView = new ResultView(ResultView.SUCCESS);
		if(majorId!=null){
			List<DirectionView> directList = commonService.findAllDirectByMajorId(majorId);
			if(directList!=null&&!directList.isEmpty()){
				resultView.setValue(directList);
				return resultView;
			}
		}
		return resultView;
	}
}
