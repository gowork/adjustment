package com.stooges.adjustment.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.stooges.adjustment.form.AccountSearchForm;
import com.stooges.adjustment.service.AdminService;
import com.stooges.adjustment.view.AccountView;
import com.stooges.adjustment.view.ResultView;

/**
 * 管理员控制器
 * @author 张超
 *
 */
@RequestMapping("/admin")
@Controller
public class AdminController {
	private static final Logger log = LoggerFactory.getLogger(AdminController.class);
	@Resource
	private AdminService adminService;
	/**
	 * 打开账户管理界面
	 * @return
	 */
	@RequestMapping("/account")
	public String account(){
		return "admin/account";
	}
	/**
	 * 查询账户所有信息
	 * @param accountSearchForm 账户查询表单
	 * @return
	 */
	@RequestMapping(value="/showaccountlist",method = RequestMethod.POST)
	@ResponseBody
	public ResultView showAccountList(@RequestBody AccountSearchForm accountSearchForm){
		ResultView resultView = new ResultView();
		//TODO 检查表单输入
		try {
			PageInfo<AccountView> accountList= adminService.findAccoutList(accountSearchForm);
			resultView.setStatus(ResultView.SUCCESS);
			resultView.setValue(accountList);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setMsg("查询有误，请稍候重试！");
			resultView.setStatus(ResultView.ERROR);
		}
		return resultView;
	}
	/**
	 * 停用账户
	 * @param localOauthId 账户ID
	 * @return
	 */
	@RequestMapping(value="/deadaccount",method = RequestMethod.POST)
	@ResponseBody
	public ResultView deadAccount(@RequestBody Long localOauthId){
		ResultView resultView = new ResultView();
		if(localOauthId==null){
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg("请选择正确的帐号");
			return resultView;
		}
		try {
			boolean deadResult= adminService.deadAccount(localOauthId);
			if(deadResult){
				resultView.setStatus(ResultView.SUCCESS);
			}else{
				resultView.setMsg("操作有误，请稍候重试！");
				resultView.setStatus(ResultView.ERROR);
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setMsg("操作有误，请稍候重试！");
			resultView.setStatus(ResultView.ERROR);
		}
		return resultView;
	}
	/**
	 * 重置密码
	 * @param localOauthId 账户ID
	 * @return
	 */
	@RequestMapping(value="/resetpassword",method = RequestMethod.POST)
	@ResponseBody
	public ResultView resetPassword(@RequestBody Long localOauthId){
		ResultView resultView = new ResultView();
		if(localOauthId==null){
			resultView.setStatus(ResultView.ERROR);
			resultView.setMsg("请选择正确的帐号");
			return resultView;
		}
		try {
			boolean deadResult= adminService.resetPassword(localOauthId);
			if(deadResult){
				resultView.setStatus(ResultView.SUCCESS);
			}else{
				resultView.setMsg("操作有误，请稍候重试！");
				resultView.setStatus(ResultView.ERROR);
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			resultView.setMsg("操作有误，请稍候重试！");
			resultView.setStatus(ResultView.ERROR);
		}
		return resultView;
	}
	/**
	 * 查询报考情况
	 * @param session
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/applycount")
	public String applyCount(HttpSession session,ModelMap modelMap){
		//查询所在学院及相关专业
//		OauthView oauthView = (OauthView)session.getAttribute(Constants.SESSION_USER);
		//查询所有学院、专业、方向
//		MajorCollectionView majorCollection = collegeService.findMajorCollectByOauthId(oauthView.getId());
//		modelMap.put("majorCollection", majorCollection);
		return "admin/apply-count";
	}
}
