package com.stooges.adjustment.view;

/**
 * 申请记录（用于教师或者学院查询所有申请列表显示用）
 * @author 张超
 *
 */
public class ApplyView {
	private Long localOauthId;           //学生ID
	private Long targetId;               //志愿ID
	private String name;                 //姓名
	private String sex;                  //性别
	private Integer status;              //状态
	private String collegeName;          //报考学院
	private String majorName;            //报考专业
	private String undergraduateSchool;  //本科学校
	private String undergraduateMajor;   //本科专业
	private String totalScore;           //总分
	private Integer admissionStatus;     //录取状态
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Long getTargetId() {
		return targetId;
	}
	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getUndergraduateSchool() {
		return undergraduateSchool;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public void setUndergraduateSchool(String undergraduateSchool) {
		this.undergraduateSchool = undergraduateSchool;
	}
	public String getUndergraduateMajor() {
		return undergraduateMajor;
	}
	public void setUndergraduateMajor(String undergraduateMajor) {
		this.undergraduateMajor = undergraduateMajor;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
	public Integer getAdmissionStatus() {
		return admissionStatus;
	}
	public void setAdmissionStatus(Integer admissionStatus) {
		this.admissionStatus = admissionStatus;
	}
}