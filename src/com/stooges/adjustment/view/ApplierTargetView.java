package com.stooges.adjustment.view;

/**
 * 学生志愿（用于在教师或者学院查询学生详情的时候用）
 * @author 张超
 *
 */
public class ApplierTargetView {
	private Long id;                 //志愿ID
	private Long localOauthId;       //本地授权ID
	private String targetCollege;    //志愿学院
	private String targetMajor;      //志愿专业
	private String targetDirection;  //志愿方向
	private Integer targetLevel;     //志愿等级
	private String status;           //状态
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTargetCollege() {
		return targetCollege;
	}
	public void setTargetCollege(String targetCollege) {
		this.targetCollege = targetCollege;
	}
	public String getTargetMajor() {
		return targetMajor;
	}
	public void setTargetMajor(String targetMajor) {
		this.targetMajor = targetMajor;
	}
	public String getTargetDirection() {
		return targetDirection;
	}
	public void setTargetDirection(String targetDirection) {
		this.targetDirection = targetDirection;
	}
	public Integer getTargetLevel() {
		return targetLevel;
	}
	public void setTargetLevel(Integer targetLevel) {
		this.targetLevel = targetLevel;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
