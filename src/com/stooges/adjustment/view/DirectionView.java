package com.stooges.adjustment.view;

/**
 * 专业方向视图
 * @author 张超
 *
 */
public class DirectionView {
	private Integer id;      //ID
	private Integer majorId; //专业ID
	private String name;     //专业方向名称
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
