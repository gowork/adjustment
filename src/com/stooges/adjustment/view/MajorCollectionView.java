package com.stooges.adjustment.view;

import java.util.List;

/**
 * 专业集合视图 
 * @author 张超
 *
 */
public class MajorCollectionView {
	private Integer collegeId;                         //学院ID
	private String collegeName;                        //学院名称
	private List<DirectCollectView> directCollectList; //专业集合
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public List<DirectCollectView> getDirectCollectList() {
		return directCollectList;
	}
	public void setDirectCollectList(List<DirectCollectView> directCollectList) {
		this.directCollectList = directCollectList;
	}
}
