package com.stooges.adjustment.view;

import java.util.List;

/**
 * 学生申请调剂全部信息(用于教师或者学院查看学生信息用)
 * @author 张超
 *
 */
public class ApplierView {
	private Long localOauthId;                   //本地授权ID
	private String name;                         //姓名
	private String tel;                          //电话
	private String ticketNumber;                 //准考证
	private String emergContactTel;              //紧急联系电话
	private String undergraduateSchool;          //本科学校
	private String undergraduateMajor;           //本科专业
	private String evaluation;                   //自我评价
	private String special;                      //特长
	private String prize;                        //获奖情况
	private String postSchoolName;               //考研报考学校
	private String postSchoolMajor;              //考研报考专业
	private String mathName;                     //数学名称
	private String mathScore;                    //数学成绩
	private String englishName;                  //英语名称
	private String englishScore;                 //英语成绩
	private String polityScore;                  //政治成绩
	private String business1Name;                //业务1名称
	private String business1Score;               //业务1成绩
	private String business2Name;                //业务2名称
	private String business2Score;               //业务2成绩
	private String totalScore;                   //总成绩
	private Integer isAcceptSwap;                //是否接受调剂
	private List<ApplierTargetView> targetList;  //志愿集合
	
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getEmergContactTel() {
		return emergContactTel;
	}
	public void setEmergContactTel(String emergContactTel) {
		this.emergContactTel = emergContactTel;
	}
	public String getUndergraduateSchool() {
		return undergraduateSchool;
	}
	public void setUndergraduateSchool(String undergraduateSchool) {
		this.undergraduateSchool = undergraduateSchool;
	}
	public String getUndergraduateMajor() {
		return undergraduateMajor;
	}
	public void setUndergraduateMajor(String undergraduateMajor) {
		this.undergraduateMajor = undergraduateMajor;
	}
	public String getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getPrize() {
		return prize;
	}
	public void setPrize(String prize) {
		this.prize = prize;
	}
	public String getPostSchoolName() {
		return postSchoolName;
	}
	public void setPostSchoolName(String postSchoolName) {
		this.postSchoolName = postSchoolName;
	}
	public String getPostSchoolMajor() {
		return postSchoolMajor;
	}
	public void setPostSchoolMajor(String postSchoolMajor) {
		this.postSchoolMajor = postSchoolMajor;
	}
	public String getMathScore() {
		return mathScore;
	}
	public void setMathScore(String mathScore) {
		this.mathScore = mathScore;
	}
	public String getEnglishScore() {
		return englishScore;
	}
	public void setEnglishScore(String englishScore) {
		this.englishScore = englishScore;
	}
	public String getPolityScore() {
		return polityScore;
	}
	public void setPolityScore(String polityScore) {
		this.polityScore = polityScore;
	}
	public String getBusiness1Score() {
		return business1Score;
	}
	public void setBusiness1Score(String business1Score) {
		this.business1Score = business1Score;
	}
	public String getBusiness2Score() {
		return business2Score;
	}
	public void setBusiness2Score(String business2Score) {
		this.business2Score = business2Score;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
	public Integer getIsAcceptSwap() {
		return isAcceptSwap;
	}
	public void setIsAcceptSwap(Integer isAcceptSwap) {
		this.isAcceptSwap = isAcceptSwap;
	}
	public List<ApplierTargetView> getTargetList() {
		return targetList;
	}
	public void setTargetList(List<ApplierTargetView> targetList) {
		this.targetList = targetList;
	}
	public String getMathName() {
		return mathName;
	}
	public void setMathName(String mathName) {
		this.mathName = mathName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getBusiness1Name() {
		return business1Name;
	}
	public void setBusiness1Name(String business1Name) {
		this.business1Name = business1Name;
	}
	public String getBusiness2Name() {
		return business2Name;
	}
	public void setBusiness2Name(String business2Name) {
		this.business2Name = business2Name;
	}
}
