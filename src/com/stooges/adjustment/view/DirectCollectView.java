package com.stooges.adjustment.view;

import java.util.List;

/**
 * 添加修改学院、专业、方向
 * @author 张超
 *
 */
public class DirectCollectView {
	private Integer majorId;                //专业ID
	private Integer collegeId;              //学院ID
	private String majorName;               //专业名称
	private List<DirectionView> directList; //专业方向集合
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public List<DirectionView> getDirectList() {
		return directList;
	}
	public void setDirectList(List<DirectionView> directList) {
		this.directList = directList;
	}
}
