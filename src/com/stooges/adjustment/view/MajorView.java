package com.stooges.adjustment.view;

/**
 * 专业视图
 * @author 张超
 *
 */
public class MajorView {
	private Integer id; //ID
	private String name;//专业名称
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
