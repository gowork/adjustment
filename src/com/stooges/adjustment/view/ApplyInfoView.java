package com.stooges.adjustment.view;

import java.util.List;

/**
 * 申请调剂信息（用于学生填写调剂信息）
 * @author 张超
 *
 */
public class ApplyInfoView {
	private Long localOauthId;           //本地授权ID
	private Integer isAcceptSwap;        //是否接受校内调剂
	private List<TargetView> targetList; //目标集合
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getIsAcceptSwap() {
		return isAcceptSwap;
	}
	public void setIsAcceptSwap(Integer isAcceptSwap) {
		this.isAcceptSwap = isAcceptSwap;
	}
	public List<TargetView> getTargetList() {
		return targetList;
	}
	public void setTargetList(List<TargetView> targetList) {
		this.targetList = targetList;
	}
}
