package com.stooges.adjustment.view;

/**
 * 页面显示用学院
 * @author 张超
 *
 */
public class CollegeView {
	private Integer id;      //ID
	private String name;     //学院名称
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
