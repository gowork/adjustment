package com.stooges.adjustment.view;

/**
 * 申请调剂的目标（用于学生申请调剂显示）
 * @author 张超
 *
 */
public class TargetView {
	private Long id;                   //目标ID
	private Integer targetCollegeId;   //申请学院ID
	private Integer targetMajorId;     //申请专业ID
	private Integer targetDirectionId; //申请方向ID
	private Integer targetLevel;       //目标优先级
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getTargetCollegeId() {
		return targetCollegeId;
	}
	public void setTargetCollegeId(Integer targetCollegeId) {
		this.targetCollegeId = targetCollegeId;
	}
	public Integer getTargetMajorId() {
		return targetMajorId;
	}
	public void setTargetMajorId(Integer targetMajorId) {
		this.targetMajorId = targetMajorId;
	}
	public Integer getTargetDirectionId() {
		return targetDirectionId;
	}
	public void setTargetDirectionId(Integer targetDirectionId) {
		this.targetDirectionId = targetDirectionId;
	}
	public Integer getTargetLevel() {
		return targetLevel;
	}
	public void setTargetLevel(Integer targetLevel) {
		this.targetLevel = targetLevel;
	}
}
