package com.stooges.adjustment.view;

/**
 * 
 *ajax请求返回的结果
 */
public class ResultView {
	public static final int SUCCESS = 1;
	public static final int ERROR = 2;
	
	private int status;
	private String msg;
	private Object value;
	private Object feature;//扩展字段
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Object getFeature() {
		return feature;
	}
	public void setFeature(Object feature) {
		this.feature = feature;
	}
	public ResultView() {}
	
	public ResultView(int status) {
		this.status = status;
	}
}
