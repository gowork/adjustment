package com.stooges.adjustment.entity;

/**
 * 学院
 * @author 张超
 *
 */
public class College {
	private Integer id;      //ID
	private String name;     //学院名称
	private Integer status;  //状态
	private Long createTime; //创建时间
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
}
