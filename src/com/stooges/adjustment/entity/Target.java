package com.stooges.adjustment.entity;

/**
 * 目标
 * @author 张超
 *
 */
public class Target {
	private Long id;                   //ID
	private Long localOauthId;         //本地授权ID
	private Integer targetCollegeId;   //目标学院ID
	private Integer targetMajorId;     //目标专业ID
	private Integer targetDirectionId; //目标专业方向ID
	private Integer targetLevel;       //目标等级  
	private Integer status;            //录取状态
	private String allLog;             //全部操作记录
	private String lastLog;            //最后一次操作记录				
	private Long createTime;           //创建时间
	private Long modifyTime;           //修改时间
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getTargetCollegeId() {
		return targetCollegeId;
	}
	public void setTargetCollegeId(Integer targetCollegeId) {
		this.targetCollegeId = targetCollegeId;
	}
	public Integer getTargetMajorId() {
		return targetMajorId;
	}
	public void setTargetMajorId(Integer targetMajorId) {
		this.targetMajorId = targetMajorId;
	}
	public Integer getTargetDirectionId() {
		return targetDirectionId;
	}
	public void setTargetDirectionId(Integer targetDirectionId) {
		this.targetDirectionId = targetDirectionId;
	}
	public Integer getTargetLevel() {
		return targetLevel;
	}
	public void setTargetLevel(Integer targetLevel) {
		this.targetLevel = targetLevel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getAllLog() {
		return allLog;
	}
	public void setAllLog(String allLog) {
		this.allLog = allLog;
	}
	public String getLastLog() {
		return lastLog;
	}
	public void setLastLog(String lastLog) {
		this.lastLog = lastLog;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Long modifyTime) {
		this.modifyTime = modifyTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localOauthId == null) ? 0 : localOauthId.hashCode());
		result = prime * result + ((targetLevel == null) ? 0 : targetLevel.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Target other = (Target) obj;
		if (localOauthId == null) {
			if (other.localOauthId != null)
				return false;
		} else if (!localOauthId.equals(other.localOauthId))
			return false;
		if (targetLevel == null) {
			if (other.targetLevel != null)
				return false;
		} else if (!targetLevel.equals(other.targetLevel))
			return false;
		return true;
	}
}
