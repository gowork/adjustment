package com.stooges.adjustment.entity;

/**
 * 登录日志
 * @author 张超
 *
 */
public class LoginLog {
	private Long id;              //ID
	private Long localOauthId;    //登录ID
	private String token;         //登录token
	private Long expires;         //超时时间
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Long getExpires() {
		return expires;
	}
	public void setExpires(Long expires) {
		this.expires = expires;
	}
}
