package com.stooges.adjustment.entity;

/**
 * 学院帐号
 * @author 张超
 *
 */
public class CollegeAccount {
	private Long id;           //ID
	private Long localOauthId; //本地授权ID
	private Integer collegeId; //学院ID
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
}
