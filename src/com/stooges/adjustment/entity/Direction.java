package com.stooges.adjustment.entity;

/**
 * 专业方向
 * @author 张超
 *
 */
public class Direction {
	private Integer id;      //ID
	private Integer majorId; //专业ID
	private String name;     //专业方向ID
	private Integer status;  //状态
	private Long createTime; //创建时间
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
}
