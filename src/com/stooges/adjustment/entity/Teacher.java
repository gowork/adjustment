package com.stooges.adjustment.entity;

/**
 * 教师
 * @author 张超
 *
 */
public class Teacher {
	private Long Id;           //ID
	private Long localOauthId; //本地授权ID
	private Integer collegeId; //学院ID
	private String name;       //真实姓名
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
