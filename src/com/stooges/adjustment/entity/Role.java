package com.stooges.adjustment.entity;

/**
 * 角色
 * @author 张超
 *
 */
public class Role {
	private Integer id;     //ID
	private String name;    //角色名称
	private Integer status; //状态
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
