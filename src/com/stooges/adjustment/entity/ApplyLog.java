package com.stooges.adjustment.entity;

/**
 * 申请日志
 * @author 张超
 *
 */
public class ApplyLog {
	private Long applierId;      //申请者ID
	private Long operateId;      //操作者ID
	private String operateDate;  //操作时间
	private String comment;      //说明
	public Long getApplierId() {
		return applierId;
	}
	public void setApplierId(Long applierId) {
		this.applierId = applierId;
	}
	public Long getOperateId() {
		return operateId;
	}
	public void setOperateId(Long operateId) {
		this.operateId = operateId;
	}
	public String getOperateDate() {
		return operateDate;
	}
	public void setOperateDate(String operateDate) {
		this.operateDate = operateDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
