package com.stooges.adjustment.form;

import java.util.List;

/**
 * 申请调剂表单
 * @author 张超
 *
 */
public class ApplyInfoForm {
	private Long localOauthId;               //本地授权ID
	private Integer isAcceptSwap;            //是否接受校内调剂
	private List<TargetForm> targetFormList; //目标
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getIsAcceptSwap() {
		return isAcceptSwap;
	}
	public void setIsAcceptSwap(Integer isAcceptSwap) {
		this.isAcceptSwap = isAcceptSwap;
	}
	public List<TargetForm> getTargetFormList() {
		return targetFormList;
	}
	public void setTargetFormList(List<TargetForm> targetFormList) {
		this.targetFormList = targetFormList;
	}
}
