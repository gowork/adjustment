package com.stooges.adjustment.form;

/***
 * 注册用表单
 * @author 张超
 *
 */
public class RegistForm {
	private String tel;          //手机
	private String ticketNumber; //准考证号
	private String idcard;       //身份证号
	private String email;        //邮箱
	private String password;     //密码
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
