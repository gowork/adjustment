package com.stooges.adjustment.form;

/**
 * 考试信息表单
 * @author 张超
 *
 */
public class ExamInfoForm {
	private Long localOauthId;       //本地授权ID
	private String postSchoolName;   //报考学校
	private String postSchoolMajor;  //报考专业
	private String mathName;         //数学名称
	private String mathScore;        //数学成绩
	private String englishName;      //外语名称
	private String englishScore;     //外语成绩
	private String polityScore;      //政治成绩
	private String business1Name;    //业务1名称
	private String business1Score;   //业务1成绩
	private String business2Name;    //业务2名称
	private String business2Score;   //业务2成绩
	private String totalScore;       //总成绩
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public String getPostSchoolName() {
		return postSchoolName;
	}
	public void setPostSchoolName(String postSchoolName) {
		this.postSchoolName = postSchoolName;
	}
	public String getPostSchoolMajor() {
		return postSchoolMajor;
	}
	public void setPostSchoolMajor(String postSchoolMajor) {
		this.postSchoolMajor = postSchoolMajor;
	}
	public String getMathScore() {
		return mathScore;
	}
	public void setMathScore(String mathScore) {
		this.mathScore = mathScore;
	}
	public String getEnglishScore() {
		return englishScore;
	}
	public void setEnglishScore(String englishScore) {
		this.englishScore = englishScore;
	}
	public String getPolityScore() {
		return polityScore;
	}
	public void setPolityScore(String polityScore) {
		this.polityScore = polityScore;
	}
	public String getBusiness1Score() {
		return business1Score;
	}
	public void setBusiness1Score(String business1Score) {
		this.business1Score = business1Score;
	}
	public String getBusiness2Score() {
		return business2Score;
	}
	public void setBusiness2Score(String business2Score) {
		this.business2Score = business2Score;
	}
	public String getMathName() {
		return mathName;
	}
	public void setMathName(String mathName) {
		this.mathName = mathName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getBusiness1Name() {
		return business1Name;
	}
	public void setBusiness1Name(String business1Name) {
		this.business1Name = business1Name;
	}
	public String getBusiness2Name() {
		return business2Name;
	}
	public void setBusiness2Name(String business2Name) {
		this.business2Name = business2Name;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}
}
