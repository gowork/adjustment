package com.stooges.adjustment.form;

/**
 * 申请条件表单
 * @author 张超
 *
 */
public class ApplyConditionForm {
	private Integer collegeId; //学院ID
	private Integer majorId;   //专业ID
	private Integer status;    //状态ID
	private Integer pageNum;   //页码
	private Integer pageSize;  //页面显示条数
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
