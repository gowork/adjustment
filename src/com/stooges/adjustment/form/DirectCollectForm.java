package com.stooges.adjustment.form;

import java.util.List;

/**
 * 所有专业表单集合
 * @author 张超
 *
 */
public class DirectCollectForm {
	private Integer majorId;                //专业ID
	private String majorName;               //专业名称
	private Integer collegeId;              //学院ID
	private List<DirectionForm> directList; //专业方向集合
	private Integer status;                 //状态
	private Long createTime;                //创建时间
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public List<DirectionForm> getDirectList() {
		return directList;
	}
	public void setDirectList(List<DirectionForm> directList) {
		this.directList = directList;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
}
