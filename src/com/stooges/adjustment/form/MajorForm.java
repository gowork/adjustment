package com.stooges.adjustment.form;

/**
 * 专业表单
 * @author 张超
 *
 */
public class MajorForm {
	private Integer collegeId; //学院ID
	private String majorName;  //专业名称
	private Long createTime;   //创建时间
	private Integer status;    //状态
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getMajorName() {
		return majorName;
	}
	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
