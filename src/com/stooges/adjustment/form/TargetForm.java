package com.stooges.adjustment.form;

/**
 * 申请调剂志愿表单
 * @author 张超
 *
 */
public class TargetForm {
	private Long localOauthId;         //本地授权ID
	private Integer targetCollegeId;   //目标学院
	private Integer targetMajorId;     //目标专业
	private Integer targetDirectionId; //目标方向
	private Integer targetLevel;        //目标优先级
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public Integer getTargetCollegeId() {
		return targetCollegeId;
	}
	public void setTargetCollegeId(Integer targetCollegeId) {
		this.targetCollegeId = targetCollegeId;
	}
	public Integer getTargetMajorId() {
		return targetMajorId;
	}
	public void setTargetMajorId(Integer targetMajorId) {
		this.targetMajorId = targetMajorId;
	}
	public Integer getTargetDirectionId() {
		return targetDirectionId;
	}
	public void setTargetDirectionId(Integer targetDirectionId) {
		this.targetDirectionId = targetDirectionId;
	}
	public Integer getTargetLevel() {
		return targetLevel;
	}
	public void setTargetLevel(Integer targetLevel) {
		this.targetLevel = targetLevel;
	}
}
