package com.stooges.adjustment.form;

/**
 * 修改学生申请表单（录取、不予录取）
 * @author 张超
 *
 */
public class ApplyAlterForm {
	private Long studentId; //学生ID
	private Long targetId;  //申请目标ID
	private Integer status; //状态
	private Long operatorId;//操作者ID
	private String allLog;  //所有操作日志
	private String lastLog; //最后一次操作日志
	private Long modifyTime;//修改时间
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public Long getTargetId() {
		return targetId;
	}
	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}
	public String getAllLog() {
		return allLog;
	}
	public void setAllLog(String allLog) {
		this.allLog = allLog;
	}
	public String getLastLog() {
		return lastLog;
	}
	public void setLastLog(String lastLog) {
		this.lastLog = lastLog;
	}
	public Long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Long modifyTime) {
		this.modifyTime = modifyTime;
	}
}
