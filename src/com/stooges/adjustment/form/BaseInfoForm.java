package com.stooges.adjustment.form;

/**
 * 学生基本信息表单
 * @author 张超
 *
 */
public class BaseInfoForm {
	private Long localOauthId;          //本地授权ID
	private String name;                //名称
	private String sex;                 //性别
	private String emergContactTel;     //紧急联系电话
	private String undergraduateSchool; //本科学校
	private String undergraduateMajor;  //本科专业
	private String evaluation;          //自我评价
	private String special;             //特长
	private String prize;               //获奖情况
	public Long getLocalOauthId() {
		return localOauthId;
	}
	public void setLocalOauthId(Long localOauthId) {
		this.localOauthId = localOauthId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmergContactTel() {
		return emergContactTel;
	}
	public void setEmergContactTel(String emergContactTel) {
		this.emergContactTel = emergContactTel;
	}
	public String getUndergraduateSchool() {
		return undergraduateSchool;
	}
	public void setUndergraduateSchool(String undergraduateSchool) {
		this.undergraduateSchool = undergraduateSchool;
	}
	public String getUndergraduateMajor() {
		return undergraduateMajor;
	}
	public void setUndergraduateMajor(String undergraduateMajor) {
		this.undergraduateMajor = undergraduateMajor;
	}
	public String getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
	public String getPrize() {
		return prize;
	}
	public void setPrize(String prize) {
		this.prize = prize;
	}
}
