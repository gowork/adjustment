package com.stooges.adjustment.form;

/**
 * 开立账户的表单
 * @author 张超
 *
 */
public class AccountForm {
	private String username;     //用户名
	private String password;     //密码
	private Integer accountType; //帐号类型
	private String name;         //真实姓名
	private Integer collegeId;   //学院ID
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
