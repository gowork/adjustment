package com.stooges.adjustment.form;

/**
 * 专业方向表单
 * @author 张超
 *
 */
public class DirectionForm {
	private Integer id;     //ID
	private String name;    //专业方向名称
	private Integer majorId;//专业ID 
	private Long createTime;//创建时间
	private Integer status; //状态
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMajorId() {
		return majorId;
	}
	public void setMajorId(Integer majorId) {
		this.majorId = majorId;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
