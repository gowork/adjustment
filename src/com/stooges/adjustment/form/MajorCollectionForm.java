package com.stooges.adjustment.form;

import java.util.List;

/**
 * 
 * @author 张超
 *
 */
public class MajorCollectionForm {
	private Integer collegeId;                         //学院ID
	private String collegeName;                        //学院名称
	private List<DirectCollectForm> directCollectList; //专业集合
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public List<DirectCollectForm> getDirectCollectList() {
		return directCollectList;
	}
	public void setDirectCollectList(List<DirectCollectForm> directCollectList) {
		this.directCollectList = directCollectList;
	}
}
