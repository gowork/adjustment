package com.stooges.adjustment.form;

/**
 * 修改密码表单
 * @author 张超
 *
 */
public class PasswordForm {
	private String newPassword;//新密码
	private String oldPassword;//旧密码
	private String confirmPassword;//确认密码
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
