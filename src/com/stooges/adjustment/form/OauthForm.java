package com.stooges.adjustment.form;

/**
 * 登录的表单
 * @author 张超
 *
 */
public class OauthForm {
	private String username;  //用户名（不仅为用户名，身份证号 准考证号等都可以）
	private String password;  //密码
	private boolean isRemeber;  //记住密码
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isRemeber() {
		return isRemeber;
	}
	public void setRemeber(boolean isRemeber) {
		this.isRemeber = isRemeber;
	}
}
