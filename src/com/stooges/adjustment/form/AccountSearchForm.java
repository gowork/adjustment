package com.stooges.adjustment.form;

/**
 * 查询账户条件
 * @author 张超
 *
 */
public class AccountSearchForm {
	private Integer collegeId; //学院ID
	private Integer roleId;    //角色ID
	private Integer status;    //状态
	private Integer pageNum;   //页码
	private Integer pageSize;  //页面显示条数
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
