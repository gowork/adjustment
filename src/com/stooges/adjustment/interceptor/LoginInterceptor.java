package com.stooges.adjustment.interceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.stooges.adjustment.entity.LoginLog;
import com.stooges.adjustment.service.LoginService;
import com.stooges.adjustment.util.Constants;
import com.stooges.adjustment.util.SimpleValidator;
import com.stooges.adjustment.util.WebUtil;
import com.stooges.adjustment.view.OauthView;

/**
 * 登录拦截器
 * @author 张超
 *
 */
public class LoginInterceptor extends HandlerInterceptorAdapter{
	private static final Logger log = LoggerFactory.getLogger(LoginInterceptor.class);

	@Resource
	private LoginService loginService;
    /**  
     * 在业务处理器处理请求之前被调用  
     * 如果返回false
     *     从当前的拦截器往回执行所有拦截器的afterCompletion(),再退出拦截器链 
     * 如果返回true  
     *    执行下一个拦截器,直到所有的拦截器都执行完毕  
     *    再执行被拦截的Controller  
     *    然后进入拦截器链,  
     *    从最后一个拦截器往回执行所有的postHandle()  
     *    接着再从最后一个拦截器往回执行所有的afterCompletion()  
     */
    @Override    
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {    
        HttpSession session = request.getSession();
        OauthView oauth = (OauthView)session.getAttribute(Constants.SESSION_USER);
		if(oauth!=null){
			return true;
		}else{
			//检查cookies中是否有登录信息
			String uid = WebUtil.getCookieValueByName(request, Constants.COOKIE_UID);
			String token = WebUtil.getCookieValueByName(request, Constants.COOKIE_TOKEN);
			if(SimpleValidator.isNotBlank(uid)&&SimpleValidator.isNotBlank(token)){
				try {
					LoginLog loginLog = new LoginLog();
					loginLog.setLocalOauthId(Long.valueOf(uid));
					loginLog.setToken(token);
					OauthView oauthView = loginService.findOauthByLoged(loginLog);
					if(oauthView!=null){
						session.setAttribute(Constants.SESSION_USER, oauthView);
						return true;
					}
				} catch (Exception e) {
					log.error(e.getMessage(),e);
				}
			}
			
			//session及cookies中都不存在登录数据
            if (request.getHeader("x-requested-with") != null 
            	&& request.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest")){ //如果是ajax请求响应头会有，x-requested-with  
                response.setHeader("sessionstatus", "timeout");//在响应头设置session状态  
            }else{
                request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);  
            }
            return false;
		}
    }    
    
    /** 
     * 在业务处理器处理请求执行完成后,生成视图之前执行的动作    
     * 可在modelAndView中加入数据，比如当前时间 
     */
    @Override
    public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {     
    }
    
    /**  
     * 在DispatcherServlet完全处理完请求后被调用,可用于清理资源等   
     *   
     * 当有拦截器抛出异常时,会从当前拦截器往回执行所有的拦截器的afterCompletion()  
     */    
    @Override    
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {    
    }
}