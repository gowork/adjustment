$(function(){
	var examInfoValidate = $('#examInfoContent').SimValidate();
	$('.btn-primary').click(function(){
		if(examInfoValidate.validate()){
			var postSchoolName = $('[name="postSchoolName"]').val();
			var postSchoolMajor = $('[name="postSchoolMajor"]').val();
			var mathScore = $('[name="mathScore"]').val();
			var englishScore = $('[name="englishScore"]').val();
			var polityScore = $('[name="polityScore"]').val();
			var business1Score = $('[name="business1Score"]').val();
			var business2Score = $('[name="business2Score"]').val();
			var totalScore = $('[name="totalScore"]').val();
			var examInfo = {};
			examInfo['postSchoolName'] = postSchoolName;
			examInfo['postSchoolMajor'] = postSchoolMajor;
			examInfo['mathName'] = $('[name="mathName"]').val();
			examInfo['mathScore'] = mathScore;
			examInfo['englishName'] = $('[name="englishName"]').val();
			examInfo['englishScore'] = englishScore;
			examInfo['polityScore'] = polityScore;
			examInfo['business1Name'] = $('[name="business1Name"]').val();
			examInfo['business1Score'] = business1Score;
			examInfo['business2Name'] = $('[name="business2Name"]').val();
			examInfo['business2Score'] = business2Score;
			examInfo['totalScore'] = totalScore;
			$.ajax({
                cache: true,
                type: 'POST',
                url:'student/saveexaminfo',
                dataType:"json",      
    			contentType:"application/json",               
                data:JSON.stringify(examInfo),
                async: false,
                error: function(request) {
                	var d = dialog({
                	    title: '提示',
                	    content: '连接失败，请稍候重试！'
                	});
                	d.show();
                },
                success: function(data) {
                	var status= data.status;
                	if(status==1){
                		var d = dialog({
                			title: '提示',
                			content: '保存成功！'
                		});
                		d.show();
                	}else{
                		var d = dialog({
                			title: '提示',
                			content: data.msg
                		});
                		d.show();
                	}
                }
            });
		}
	});
});