$(function(){
	var baseInfoValidate = $('#baseInfoContent').SimValidate();
	$('.btn-primary').click(function(){
		var name = $('[name="name"]').val();
		var sex = $('[name="sex"]').val();
		var emergContactTel = $('[name="emergContactTel"]').val();
		var undergraduateSchool = $('[name="undergraduateSchool"]').val();
		var undergraduateMajor = $('[name="undergraduateMajor"]').val();
		var evaluation = $('[name="evaluation"]').val();
		var special = $('[name="special"]').val();
		var prize = $('[name="prize"]').val();
		var baseInfo = {};
		baseInfo['name'] = name;
		baseInfo['sex'] = sex;
		baseInfo['emergContactTel'] = emergContactTel;
		baseInfo['undergraduateSchool'] = undergraduateSchool;
		baseInfo['undergraduateMajor'] = undergraduateMajor;
		baseInfo['evaluation'] = evaluation;
		baseInfo['special'] = special;
		baseInfo['prize'] = prize;
		
		if(baseInfoValidate.validate()){
			$.ajax({
                cache: true,
                type: 'POST',
                url:'student/savebaseinfo',
                dataType:"json",      
    			contentType:"application/json",               
                data:JSON.stringify(baseInfo),
                async: false,
                error: function(request) {
                	var d = dialog({
                	    title: '提示',
                	    content: '连接失败，请稍候重试！'
                	});
                	d.show();
                },
                success: function(data) {
                	var status= data.status;
                	if(status==1){
                		var d = dialog({
                			title: '提示',
                			content: '保存成功！'
                		});
                		d.show();
                	}else{
                		var d = dialog({
                			title: '提示',
                			content: data.msg
                		});
                		d.show();
                	}
                }
            });
		}
	});
});