$(function(){
	initSelect();
	//学院改变
	$('#targetCollegeId1,#targetCollegeId2').change(function(){
		var $this = $(this);
		$.ajax({
            cache: true,
            type: 'POST',
            url:'findAllMajorByCollegeId',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify($this.val()),
            async: false,
            error: function(request) {
            },
            success: function(data) {
            	if(data.status==1){
            		var majorHtml = [];
            		majorHtml.push('<option value="">--请选择专业--</option>');
            		$.each(data.value, function(i, item) {
        				majorHtml.push('<option value="'+item.id+'">'+item.name+'</option>'); 
            		});
            		$this.next().html(majorHtml.join(''));
            		
            		//清空方向选项
            		var directHtml = [];
					directHtml.push('<option value="">--请选择方向--</option>');
					$this.next().next().html(directHtml.join(''));
            	}
            }
          });
	});
	//专业改变
	$('#targetMajorId1,#targetMajorId2').change(function(){
		var $this = $(this);
		$.ajax({
			cache: true,
			type: 'POST',
			url:'findAllDirectByMajorId',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify($this.val()),
			async: false,
			error: function(request) {
			},
			success: function(data) {
				if(data.status==1){
					var directHtml = [];
					directHtml.push('<option value="">--请选择方向--</option>');
					if(data.value){
						$.each(data.value, function(i, item) {
							directHtml.push('<option value="'+item.id+'">'+item.name+'</option>'); 
						});
					}
					$this.next().html(directHtml.join(''));
				}
			}
		});
	});
	
	$('.btn-primary').click(function(){
		var applyInfoForm = {};
		applyInfoForm['isAcceptSwap'] = $('[name="isAcceptSwap"]:checked').val(); 
		applyInfoForm['targetFormList'] = [];
		var targetCollegeId1 = $('#targetCollegeId1').val();
		var targetMajorId1 = $('#targetMajorId1').val();
		var targetDirectionId1 = $('#targetDirectionId1').val();
		var targetCollegeId2 = $('#targetCollegeId2').val();
		var targetMajorId2 = $('#targetMajorId2').val();
		var targetDirectionId2 = $('#targetDirectionId2').val();
		var target1 = {};
		target1['targetCollegeId'] = targetCollegeId1;
		target1['targetMajorId'] = targetMajorId1;
		target1['targetDirectionId'] = targetDirectionId1;
		target1['targetLevel'] = 1;
		var target2 = {};
		target2['targetCollegeId'] = targetCollegeId2;
		target2['targetMajorId'] = targetMajorId2;
		target2['targetDirectionId'] = targetDirectionId2;
		target2['targetLevel'] = 2;
		applyInfoForm['targetFormList'].push(target1);
		applyInfoForm['targetFormList'].push(target2);
		if(!targetCollegeId1){
			showDialog('错误提示','请选择第一志愿学院');
			return;
		}
		if(!targetMajorId1){
			showDialog('错误提示','请选择第一志愿专业');
			return;
		}
		if(!targetCollegeId2){
			showDialog('错误提示','请选择第二志愿学院');
			return;
		}
		if(!targetMajorId2){
			showDialog('错误提示','请选择第二志愿专业');
			return;
		}
		if(targetCollegeId1==targetCollegeId2 && targetMajorId1==targetMajorId2 && targetDirectionId1==targetDirectionId2){
			showDialog('错误提示','第一志愿和第二志愿重复');
			return;
		}
		
		$.ajax({
            cache: true,
            type: 'POST',
            url:'/student/saveapplyinfo',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify(applyInfoForm),
            async: false,
            error: function(request) {
            	showDialog('提示','连接失败，请稍候重试！');
            },
            success: function(data) {
            	var status= data.status;
            	if(status==1){
            		showDialog('提示','保存成功！');
            	}else{
            		showDialog('提示',data.msg);
            	}
            }
        });
	});
});
/**
 * 初始化选择框
 */
function initSelect(){
	var targetCollegeId1 = $('#targetCollegeId1').data('value');
	var targetMajorId1 = $('#targetMajorId1').data('value');
	var targetDirectionId1 = $('#targetDirectionId1').data('value');
	var targetCollegeId2 = $('#targetCollegeId2').data('value');
	var targetMajorId2 = $('#targetMajorId2').data('value');
	var targetDirectionId2 = $('#targetDirectionId2').data('value');
	//初始化第一志愿
	changeCollege($('#targetCollegeId1'));
	if(targetCollegeId1){
		$('#targetCollegeId1').val(targetCollegeId1);
		changeCollege($('#targetCollegeId1'));
	}
	if(targetMajorId1){
		$('#targetMajorId1').val(targetMajorId1);
		changeMajor($('#targetMajorId1'));
	}
	if(targetDirectionId1){
		$('#targetDirectionId1').val(targetDirectionId1);
	}
	
	//初始化第二志愿
	changeCollege($('#targetCollegeId2'));
	if(targetCollegeId2){
		$('#targetCollegeId2').val(targetCollegeId2);
		changeCollege($('#targetCollegeId2'));
	}
	if(targetMajorId2){
		$('#targetMajorId2').val(targetMajorId2);
		changeMajor($('#targetMajorId2'));
	}
	if(targetDirectionId2){
		$('#targetDirectionId2').val(targetDirectionId2);
	}
}
function changeCollege(e){
	var $this = $(e);
	$.ajax({
        cache: true,
        type: 'POST',
        url:'findAllMajorByCollegeId',
        dataType:"json",      
		contentType:"application/json",               
        data:JSON.stringify($this.val()),
        async: false,
        error: function(request) {
        },
        success: function(data) {
        	if(data.status==1){
        		var majorHtml = [];
        		majorHtml.push('<option value="">--请选择专业--</option>');
        		$.each(data.value, function(i, item) {
    				majorHtml.push('<option value="'+item.id+'">'+item.name+'</option>'); 
        		});
        		$this.next().html(majorHtml.join(''));
        		
        		//清空方向选项
        		var directHtml = [];
				directHtml.push('<option value="">--请选择方向--</option>');
				$this.next().next().html(directHtml.join(''));
        	}
        }
      });
}
function changeMajor(e){
	var $this = $(e);
	$.ajax({
		cache: true,
		type: 'POST',
		url:'findAllDirectByMajorId',
        dataType:"json",      
		contentType:"application/json",               
        data:JSON.stringify($this.val()),
		async: false,
		error: function(request) {
		},
		success: function(data) {
			if(data.status==1){
				var directHtml = [];
				directHtml.push('<option value="">--请选择方向--</option>');
				if(data.value){
					$.each(data.value, function(i, item) {
						directHtml.push('<option value="'+item.id+'">'+item.name+'</option>'); 
					});
				}
				$this.next().html(directHtml.join(''));
			}
		}
	});
}
function showDialog(title,content){
	var diag = dialog({
	    title: title,
	    content: content
	});
	diag.show();
	setTimeout(function () {
		diag.close().remove();
    }, 2000);
}