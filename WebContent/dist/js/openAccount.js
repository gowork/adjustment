$(function(){
	var openAccountValidate = $('#openAccountContent').SimValidate();
	$('.btn-primary').click(function(){
		if(openAccountValidate.validate()){
			var accountForm = {};
			accountForm['accountType'] = $('[name="accountType"]').val();
			accountForm['name'] = $('[name="name"]').val();
			accountForm['username'] = $('[name="username"]').val();
			accountForm['password'] = $('[name="password"]').val();
			$.ajax({
                cache: true,
                type: 'POST',
                url:'college/openaccount',
                dataType:"json",      
    			contentType:"application/json",               
                data:JSON.stringify(accountForm),
                async: false,
                error: function(request) {
                	var d = dialog({
                	    title: '提示',
                	    content: '连接失败，请稍候重试！'
                	});
                	d.show();
                },
                success: function(data) {
                	var status= data.status;
                	if(status==1){
                		var d = dialog({
                			content: '保存成功！'
                		});
                		d.show();
                		setTimeout(function(){d.close().remove()},2000);
                	}else{
                		var d = dialog({
                			content: data.msg
                		});
                		d.show();
                		setTimeout(function(){d.close().remove()},2000);
                	}
                }
            });
		}
	});
});