<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<base href="${ctx}">
<link rel="stylesheet" href="dist/css/ui-dialog.css">
<link rel="stylesheet" href="dist/css/user.css">
<script src="dist/js/jquery2.1.4.min.js"></script>
<script src="dist/js/dialog-min.js"></script>
<script type="text/javascript">
$(function(){
	$('#user-center').mouseover(function() {
		$('#user-center .info-box').show();
	}).mouseout(function(){
		$('#user-center .info-box').hide();
	});
	$('.logout').click(function(){
		dialog({
		    content: '真的要离开吗？',
		    okValue: '确定',
		    ok: function () {
		        window.location.href='logout';
		    },
		    cancelValue: '取消',
		    cancel: function () {}
		}).width(320).showModal();
	});
});
</script>
<div class="global-nav">
	<nav class="container">
		<div class="logo"><a href=""></a></div>
		<div class="container-right">
			<a href="http://www.shiep.edu.cn/" target="_blank">上海电力学院</a>&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="http://yjsc.web.shiep.edu.cn/" target="_blank">上海电力学院研究生处</a>
		</div>
		<div style="clear:both;"></div>
	</nav>
</div>

<div id="user-center" class="ui3-user-center-wrap" style="visibility: visible;">
	<div class="avatar-abstract">
		<img data-size="cover" src="dist/img/background/user.jpg" alt="头像" width="35" style="top: 0px;" />
	</div>
	<div class="info-box clearfix">
		<div class="arrow"></div>
		<div class="detail-info-box">
			<div class="up-zone">
				<div style="padding: 14px 15px 0" class="clearfix">
					<div class="left avatar">
						<img data-size="cover" src="dist/img/background/user.jpg" width="48" alt="头像" style="top: 0px;" />
					</div>
					<div class="right user-info">
						<div class="username">${user.username}</div>
						<a href="javascript:void(0);" class="logout">退出</a>
						<div style="">
							<a href="" class="btn enter-user-center"> 进入我的主页 </a>
						</div>
					</div>
				</div>
			</div>
			<div class="down-zone clearfix" style="padding-top: 5px">
				<div class="container">
					<c:if test="${user.role.id eq 1}">
						<div class="my-link col-2 my-fav">
							<a href="admin/account">账户管理</a>
						</div>
						<div class="my-link col-2 split">
							<a href="tochangepassword">修改密码</a>
						</div>
					</c:if>
					<c:if test="${user.role.id eq 2}">
						<div class="my-link col-3 my-fav">
							<a href="college/applycount">报考情况</a>
						</div>
						<div class="my-link col-3 split">
							<a href="college/toopenaccount">开立账户</a>
						</div>
						<div class="my-link col-3 split">
							<a href="tochangepassword">修改密码</a>
						</div>
					</c:if>
					<c:if test="${user.role.id eq 3}">
						<div class="my-link col-3 my-fav">
							<a href="teacher/applycount">报考情况</a>
						</div>
						<div class="my-link col-3 split">
							<a href="teacher/toopenaccount">开立账户</a>
						</div>
						<div class="my-link col-3 split">
							<a href="tochangepassword">修改密码</a>
						</div>
					</c:if>
					<c:if test="${user.role.id eq 4}">
						<div class="my-link col-2 my-fav">
							<a href="teacher/applycount">报考情况</a>
						</div>
						<div class="my-link col-2 split">
							<a href="tochangepassword">修改密码</a>
						</div>
					</c:if>
					<c:if test="${user.role.id eq 5}">
						<div class="my-link col-4 my-fav">
							<a href="student/baseinfo">基本信息</a>
						</div>
						<div class="my-link col-4 split">
							<a href="student/examinfo">成绩</a>
						</div>
						<div class="my-link col-4 split">
							<a href="student/applyinfo">调剂</a>
						</div>
						<div class="my-link col-4 split">
							<a href="tochangepassword">修改密码</a>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>