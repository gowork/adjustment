<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>学生报考情况</title>
	<link rel="stylesheet" href="dist/css/apply-count.css">
	<link rel="stylesheet" href="dist/css/SimPage1.1.css">
	<style type="text/css">
		/**详情弹框*/
		.mod-target dl {
		    width: 734px;
		    margin: 0;
		    padding: 0;
		    display: inline-block;
		}
		.mod-target dt {
		    font-size: 14px;
		    height: 16px;
		    background: no-repeat;
		    color: #666;
		    vertical-align: baseline;
		    padding-left: 20px;
		    margin-bottom: 13px;
		    width: 300px;
		    background-position: 0 0;
		    margin: 0;
		    padding: 0;
		}
		.mod-target dd {
		    line-height: 24px;
		    padding-left: 20px;
		    margin: 0;
		    padding: 0;
		}
		.mod-target .target-attr {
		    color: #999;
		    width: 50px;
		    text-align: right;
		    padding-right: 10px;
		    display: inline-block;
		}
		.mod-target span {
		    color: #666;
		}

	</style>
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div style="clear:both;"></div>
	<div class="body-content">
		<div class="search">
			<table class="search-table">
				<tbody>
					<tr>
						<td>
							<label for="" class="search-title">学院：</label>
							<select id="collegeId" class="search-input">
								<option value="">全部</option>
								<option value="1">能源与机械工程学院</option>
								<option value="2">电气工程学院</option>
								<option value="3">计算机科学与技术学院</option>
								<option value="4">经济与管理学院</option>
								<option value="5">外国语学院</option>
								<option value="6">成人教育学院</option>
								<option value="7">环境与化学工程学院</option>
								<option value="8">自动化工程学院</option>
								<option value="9">电子与信息工程学院</option>
								<option value="10">数理学院</option>
								<option value="11">国际交流学院</option>
							</select>
						</td>
						<td>
							<label for="" class="search-title">专业：</label>
							<select id="majorId" class="search-input">
								<option value="">全部</option>
							</select>
						</td>
						<td>
							<label for="" class="search-title">录取状态：</label>
							<select id="status" class="search-input">
								<option value="">全部</option>
								<option value="1">待定</option>
								<option value="2">已录取</option>
								<option value="3">未录取</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="student-list">
			<table>
				<thead>
					<tr>
						<th>姓名</th>
						<th>性别</th>
						<th>本科学校</th>
						<th>本科专业</th>
						<th>报考学院</th>
						<th>报考专业</th>
						<th>考研总分</th>
						<th>状态</th>
						<th style="width: 130px;">操作 <a target="_blank" id="downloadExcel" style="color: #3385ff;">下载表格</a></th>
					</tr>
				</thead>
				<tbody id="applyList">
					<tr>
						<td class="no-data" colspan="9">暂无数据</td>
					</tr>
				</tbody>
			</table>
			<div id="pagePanel"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<form id="downloadForm" action="teacher/downloadapplys" method="post" target="_blank">
		<input type="hidden" name="collegeId" value="">
		<input type="hidden" name="majorId" value="">
		<input type="hidden" name="status" value="">
	</form>
<script type="text/javascript" src="dist/js/jquery.livequery.js"></script>
<script type="text/javascript" src="dist/js/SimPage1.1.js"></script>
<script type="text/javascript">
$(function(){
	$('#downloadExcel').click(function(){
		$('#downloadForm [name="collegeId"]').val($('#collegeId').val());
		$('#downloadForm [name="majorId"]').val($('#majorId').val());
		$('#downloadForm [name="status"]').val($('#status').val());
		$('#downloadForm').submit();
	});
	
	$('#collegeId').change(function(){
		
	});
	var sp=$("#pagePanel").SimPage({
		   url: 'admin/showapplylist',
		   requestNameOfPageNum:'pageNum',  
		   requestNameOfPageSize:'pageSize',
		    pageSize:10,
		    data:{collegeId:$('#collegeId').val(),majorId:$('#majorId').val(),status:$('#status').val()},
		    method:'post',
		    length:10,                        
		    onPageChange:function(data){
		    	var applyListHtml = [];
		    	if(data.status==1){
			    	var pageInfo = data.value;
			    	$.each(pageInfo, function(i, item) {
				    	applyListHtml.push('<tr>');
				    	applyListHtml.push('	<td>'+item.name+'</td>');
				    	applyListHtml.push('	<td>'+item.sex+'</td>');
				    	applyListHtml.push('	<td>'+item.undergraduateSchool+'</td>');
				    	applyListHtml.push('	<td>'+item.undergraduateMajor+'</td>');
				    	applyListHtml.push('	<td>'+item.collegeName+'</td>');
				    	applyListHtml.push('	<td>'+item.majorName+'</td>');
				    	applyListHtml.push('	<td>'+item.totalScore+'</td>');
				    	if(item.admissionStatus==1){
					    	applyListHtml.push('	<td>已录取</td>');
				    	}else{
				    		if(item.status==1){
						    	applyListHtml.push('	<td>待定</td>');
				    		}else if(item.status==2){
						    	applyListHtml.push('	<td>已录取</td>');
				    		}else if(item.status==3){
						    	applyListHtml.push('	<td>未录取</td>');
				    		}else if(item.status==4){
						    	applyListHtml.push('	<td>取消</td>');
				    		}else{
						    	applyListHtml.push('	<td>未知</td>');
				    		}
				    	}
				    	
				    	applyListHtml.push('	<td>');
				    	applyListHtml.push('		<button data-student-id="'+item.localOauthId+'" class="check">查看</button>&nbsp;');
				    	applyListHtml.push('	</td>');
				    	applyListHtml.push('</tr>');
			    	});
		    	}else{
		    		applyListHtml.push('<tr><td class="no-data" colspan="9">暂无数据</td></tr>');
		    	}
		    	$('#applyList').html(applyListHtml);
		    }
		});
	$('#majorId,#status').change(function(){
       	sp.refresh(1, {collegeId:$('#collegeId').val(),majorId:$('#majorId').val(),status:$('#status').val()});
	});
	$('.check').livequery('click',function(){
		var $this = $(this);
		var buttons= [{value: '关闭'}];
		var contentHtml = [];
		$.ajax({
            cache: true,
            type: 'POST',
            url:'admin/showapplierdetail',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify(studentId),
            async: false,
            success: function(data) {
            	if(data.status==1){
            		var applier = data.value;
					contentHtml.push('<div class="mod-target">');
					contentHtml.push('<dl> ');
					contentHtml.push('	<dt>详细信息  </dt>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">姓名</span> <span>'+applier.name+'</span>  ');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">电话</span> <span>'+applier.tel+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">紧急联系电话</span> <span>'+applier.emergContactTel+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">本科学校</span> <span>'+applier.undergraduateSchool+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">本科专业</span> <span>'+applier.undergraduateMajor+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">考研报考学校</span> <span>'+applier.postSchoolName+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">考研报考专业</span> <span>'+applier.postSchoolMajor+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">考研成绩</span> <span>数学：'+applier.mathScore+'  英语：'+applier.englishScore+'  政治：'+applier.polityScore+'  专业课：'+applier.business1Score+'  总分：'+applier.totalScore+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">自我评价</span> <span>'+applier.evaluation+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">特长</span> <span>'+applier.special+'</span>');
					contentHtml.push('	</dd>');
					contentHtml.push('	<dd>');
					contentHtml.push('		<span class="target-attr">是否接受校内调剂</span> <span>'+applier.isAcceptSwap+'</span>');
					contentHtml.push('	</dd>');
					$.each(applier.targetList,function(i,item){
						var targetTitle = '';
						var targetStatus = '';
						if(item.status==1){
							targetStatus = '待定';
						}else if(item.status==2){
							targetStatus = '已录取';
						}else if(item.status==3){
							targetStatus = '未录取';
						}else if(item.status==4){
							targetStatus = '取消';
						}
						if(item.targetLevel==1){
							contentHtml.push('	<dd>');
							contentHtml.push('		<span class="target-attr">第一志愿</span> <span>'+item.targetCollege+'  '+item.targetMajor+'  '+item.targetDirection+'  '+targetStatus+'  '+'</span>');
							contentHtml.push('	</dd>');
						}else{
							contentHtml.push('	<dd>');
							contentHtml.push('		<span class="target-attr">第二志愿</span> <span>'+item.targetCollege+'  '+item.targetMajor+'  '+item.targetDirection+'  '+targetStatus+'  '+'</span>');
							contentHtml.push('	</dd>');
						}
					});
					contentHtml.push('</dl>');
					contentHtml.push('</div>');
            		var d = dialog({title:'报考学生详细信息',content:contentHtml.join(''),button: buttons});
            		d.content(data.msg).showModal();
            	}
            }
          });
	});
});
</script>
</body>
</html>