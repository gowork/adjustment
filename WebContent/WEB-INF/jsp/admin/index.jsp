<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>管理员首页</title>
	<link rel="stylesheet" href="dist/css/admin-index.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="body-content">
		<div class="content-row">
			<div class="content-main-benefit">
				<!-- <div class="content-main-benefit-pannel" data-href="/admin/applyinfo">
					<div>
						<div class="content-main-benefit-pic"><img src="/dist/img/icons/btn-applyinfo.png" alt=""></div>
						<h2>查看报考情况</h2>
						<p>统计迄今为止的学生报考情况</p>
					</div>
				</div> -->
				<div class="content-main-benefit-pannel" data-href="admin/account">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-account.png" alt=""></div>
						<h2>账号管理</h2>
						<p>管理所有的账户</p>
					</div>
				</div>
				<!-- <div class="content-main-benefit-pannel" data-href="/admin/entrymajor">
					<div>
						<div class="content-main-benefit-pic"><img src="/dist/img/icons/btn-major.png" alt=""></div>
						<h2>录入专业信息</h2>
						<p>录入专业及专业方向</p>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<form id="pannelSubmit" action="/" method="post"></form>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.content-main-benefit-pannel').click(function(){
				var href = $(this).data('href');
				$('#pannelSubmit').attr('action',href);
				$('#pannelSubmit').submit();
			});
		});
	</script>
</body>
</html>