<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>帐号管理</title>
	<link rel="stylesheet" href="dist/css/apply-count.css">
	<link rel="stylesheet" href="dist/css/SimPage1.1.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div style="clear:both;"></div>
	<div class="body-content">
		<div class="search">
			<table class="search-table">
				<tbody>
					<tr>
						<td>
							<label for="" class="search-title">角色：</label>
							<select id="roleId" class="search-input">
								<option value="">全部</option>
								<option value="2">学院</option>
								<option value="3">教师（可开新账户）</option>
								<option value="4">教师</option>
								<option value="5">学生</option>
							</select>
						</td>
						<td>
							<label for="" class="search-title">学院：</label>
							<select id="collegeId" class="search-input">
								<option value="">全部</option>
								<option value="1">能源与机械工程学院</option>
								<option value="2">电气工程学院</option>
								<option value="3">计算机科学与技术学院</option>
								<option value="4">经济与管理学院</option>
								<option value="5">外国语学院</option>
								<option value="6">成人教育学院</option>
								<option value="7">环境与化学工程学院</option>
								<option value="8">自动化工程学院</option>
								<option value="9">电子与信息工程学院</option>
								<option value="10">数理学院</option>
								<option value="11">国际交流学院</option>
							</select>
						</td>
						<td>
							<label for="" class="search-title">状态：</label>
							<select id="status" class="search-input">
								<option value="">全部</option>
								<option value="1">正常</option>
								<option value="2">停用</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="student-list">
			<table>
				<thead>
					<tr>
						<th>用户名</th>
						<th>准考证</th>
						<th>手机</th>
						<th>邮箱</th>
						<th>角色</th>
						<th>学院</th>
						<th>状态</th>
						<th style="width: 130px;">操作</th>
					</tr>
				</thead>
				<tbody id="accountList">
					<tr>
						<td class="no-data" colspan="7">暂无数据</td>
					</tr>
				</tbody>
			</table>
			<div id="pagePanel"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
<script type="text/javascript" src="dist/js/jquery.livequery.js"></script>
<script type="text/javascript" src="dist/js/SimPage1.1.js"></script>
<script type="text/javascript">
$(function(){
	var sp=$("#pagePanel").SimPage({
		   url: 'admin/showaccountlist',
		   requestNameOfPageNum:'pageNum',  
		   requestNameOfPageSize:'pageSize',
		    pageSize:10,
		    data:{collegeId:$('#collegeId').val(),roleId:$('#roleId').val(),status:$('#status').val()},
		    method:'post',
		    length:10,                        
		    onPageChange:function(data){
		    	var accountListHtml = '';
		    	if(data.status==1){
			    	var pageInfo = data.value.list;
			    	$.each(pageInfo, function(i, item) {
			    		accountListHtml += '<tr>';
			    		accountListHtml += '	<td>'+(item.username?item.username:'')+'</td>';
			    		accountListHtml += '	<td>'+(item.ticketNumber?item.ticketNumber:'')+'</td>';
			    		accountListHtml += '	<td>'+(item.tel?item.tel:'')+'</td>';
			    		accountListHtml += '	<td>'+(item.email?item.email:'')+'</td>';
			    		accountListHtml += '	<td>'+(item.roleName?item.roleName:'')+'</td>';
			    		accountListHtml += '	<td>'+(item.collegeName?item.collegeName:'')+'</td>';
			    		if(item.status==1){
				    		accountListHtml += '	<td>正常</td>';
			    		}else{
			    			accountListHtml += '	<td>停用</td>';
			    		}
			    		accountListHtml += '	<td>';
				    	if(item.status==1){
				    		accountListHtml += '		<button data-id="'+(item.localOauthId?item.localOauthId:'')+'" class="in">停用</button>&nbsp;';
				    		accountListHtml += '		<button data-id="'+(item.localOauthId?item.localOauthId:'')+'" class="out">重置密码</button>';
				    	}else{
				    		accountListHtml += '		&nbsp;';
				    	}
				    	accountListHtml += '	</td>';
				    	accountListHtml += '</tr>';
			    	});
		    	}else{
		    		accountListHtml += '<tr><td class="no-data" colspan="7">暂无数据</td></tr>';
		    	}
		    	$('#accountList').html(accountListHtml);
		    }
		});
	changeList(sp);
	//停用
	$('.in').livequery('click',function(){
		var $this = $(this);
		var localOauthId = $(this).data('id');
		$.ajax({
            cache: true,
            type: 'POST',
            url:'admin/deadaccount',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify(localOauthId),
            async: false,
            success: function(data) {
            	if(data.status==1){
            		$this.parent().prev().html('停用');
            		$this.next().hide();
            		$this.hide();
            	}else{
            		var d = dialog();
            		d.content(data.msg).show();
            		setTimeout(function(){
            			d.close().remove();
            		},3000);
            	}
            }
          });
	});
	//重置密码
	$('.out').livequery('click',function(){
		var $this = $(this);
		var localOauthId = $(this).data('id');
		$.ajax({
            cache: true,
            type: 'POST',
            url:'admin/resetpassword',
            dataType:"json",      
			contentType:"application/json",               
            data:JSON.stringify(localOauthId),
            async: false,
            success: function(data) {
            	if(data.status==1){
            		var d = dialog();
            		d.content('密码已重置').show();
            		setTimeout(function(){
            			d.close().remove();
            		},3000);
            	}else{
            		var d = dialog();
            		d.content(data.msg).show();
            		setTimeout(function(){
            			d.close().remove();
            		},3000);
            	}
            }
          });
	});
	$('#collegeId,#roleId,#status').change(function(){
		changeList(sp);
	});
	function changeList(sp){
		var roleId = $('#roleId').val();
		var collegeId = $('#collegeId').val();
		var status = $('#status').val();
		if(roleId&&roleId==5){
			$('#collegeId').val('');
			collegeId=null;
		}
       	sp.refresh(1, {collegeId:collegeId,roleId:roleId,status:status});
	}
});
</script>
</body>
</html>