<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>教师主页</title>
	<link rel="stylesheet" href="dist/css/teacher-index.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="body-content">
		<div class="content-row">
			<div class="content-main-benefit">
				<div class="content-main-benefit-pannel" data-href="teacher/applycount">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-applyinfo.png" alt=""></div>
						<h2>查考报考情况</h2>
						<p>统计距今为止的学生报考情况</p>
					</div>
				</div>
				<c:if test="${user.role.id eq 3}">
				<div class="content-main-benefit-pannel" data-href="teacher/toopenaccount">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-account.png" alt=""></div>
						<h2>开立账号</h2>
						<p>为老师开账户</p>
					</div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<form id="pannelSubmit" action="/" method="post"></form>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.content-main-benefit-pannel').click(function(){
				var href = $(this).data('href');
				$('#pannelSubmit').attr('action',href);
				$('#pannelSubmit').submit();
			});
		});
	</script>
</body>
</html>