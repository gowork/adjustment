<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>专业介绍</title>
	<link rel="stylesheet" href="dist/css/majors.css">
</head>
<body>
	<div class="global-nav">
		<nav class="container">
			<div class="logo"></div>
			<div class="container-right">
				<a href="" target="_blank">上海电力学院</a>
			</div>
			<div style="clear:both;"></div>
		</nav>
	</div>
	<div class="wrap">
		<div class="container">
			<div class="container-right">
				<h1 class="container-tilte">专业介绍</h1>
				<!-- 专业集合start -->
				<div class="major-list">
					<!-- 每个专业start -->
					<div id="major1" class="major first-major">
						<h3 class="major-title">
							可再生能源科学与工程 代码：0807Z1
						</h3>
						<div class="major-section">
							<div class="section-content">可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学与工程是围绕可再生能源科学
							与工程是围绕可再生能源科学与工程是围绕</div>
							<div class="section-content">联系人：朱燕艳</div>
							<div class="section-content">联系电话：021-3503922 E-mail：yyzhu@shiep.edu.cn</div>
							<div class="section-content">学院网站：<a href="">http://slx.shiep.edu.cn</a></div>
						</div>
					</div>
					<!-- 每个专业end -->
				</div>
				<!-- 专业集合end -->
			</div>
			<div style="clear:both"></div>
		</div>

	</div>
	<div class="body-footer">
		<div class="footer">			
		    主管单位：上海电力学院　　主办单位：
	    	<a href="" target="_blank">上海电力学院研究生调剂系统</a>
	    	<a href="" style="color:#C00;" target="_blank">沪ICP证030485号</a>
	    	<br>
    		承办单位：上海电力学院　　　服务热线：021-82299588　　　客服邮箱：rrr@143.com
		</div>
	</div>
</body>
</html>