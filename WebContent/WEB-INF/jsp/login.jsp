<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>登录-上海电力学院预调剂系统</title>
	<link rel="stylesheet" href="dist/css/login.css">
</head>
<body>
	<!-- 头部 -->
	<div class="login-header">
		<div class="login-container clearfix">
			<div class="login-header-grid grid clearfix alpha">
		      	<h1>上海电力学院研究生预调剂系统</h1>
		      	<div class="login-logo-grid grid">
		      		<span class="logo_chsi logo_chsi_yz">&nbsp;</span>
		      	</div>
	    	</div>
			<div class="login-topright-grid grid clearfix">
				<div class="top-right">
					<a href="http://www.shiep.edu.cn/" target="_blank">上海电力学院</a>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="http://yjsc.web.shiep.edu.cn/" target="_blank">上海电力学院研究生处</a>
				</div>
			</div>
		</div>
	</div>

	<!-- 中间部分 -->
	<div class="login-centerBg">
		<div class="login-center">
			<div class="login-container clearfix">
				<div class="login-center-grid grid clearfix">					
					<div class="content-left">
						<p style="color:#f4fdff;">&nbsp;</p>
						<div class="clearfix">
							<div class="content-block">
								<ul class="zy-list">
					              	<li><a href="major/1" target="_blank">工程热物理</a></li>
					                <li><a href="major/1" target="_blank">热能工程</a></li>
					                <li><a href="major/1" target="_blank">动力机械及工程</a></li>
					                <li><a href="major/1" target="_blank">化学工艺</a></li>
					              	<li><a href="major/1" target="_blank">应用化学</a></li>
					                <li><a href="major/1" target="_blank">材料化学工程</a></li>
					                <li><a href="major/1" target="_blank">环境化学工程</a></li>
					                <li><a href="major/1" target="_blank">电力系统及其自动化</a></li>
				            	</ul>
							</div>
							<div class="content-block">
								<ul class="zy-list">
					              	<li><a href="major/1" target="_blank">电力电子与电力传动</a></li>
					                <li><a href="major/1" target="_blank">电机与电器</a></li>
					                <li><a href="major/1" target="_blank">电气系统检测与控制</a></li>
					                <li><a href="major/1" target="_blank">电工理论与新技术</a></li>
					              	<li><a href="major/1" target="_blank">智能电网信息与通信工程</a></li>
					                <li><a href="major/1" target="_blank">电力工程经济与管理</a></li>
					                <li><a href="major/1" target="_blank">可再生能源科学与工程</a></li>
					                <li><a href="major/1" target="_blank">电力信息技术</a></li>
				            	</ul>
							</div>
							<div class="content-block content-block-end">
								<ul class="zy-list">
					              	<li><a href="major/1" target="_blank">动力工程</a></li>
					                <li><a href="major/1" target="_blank">电气工程</a></li>
					                <li><a href="major/1" target="_blank">控制工程</a></li>
					                <li><a href="major/1" target="_blank">工程管理</a></li>
				            	</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="grid">&nbsp;</div>
				<div class="login-right-grid grid clearfix">
					<div id="loginValidate" class="content-right">
						<h3>请输入预调剂系统帐号进行登录</h3>
						<div class="cr-top">
							<form id="loginForm" action="login" autocomplete="off" method="post">
								<div class="ct-input">
									<span class="ct-img-yhm">&nbsp;</span>
									<input type="text" class="input-text" name="username" value="${oauthForm.username}" test-id="username" required placeholder="用户名/准考证号/身份证号">
								</div>
								<div class="ct-input" message-for="username" test-type="required">用户名不能为空</div>
								<div class="ct-input">
									<span class="ct-img-mm">&nbsp;</span>
									<input type="password" class="input-text" name="password" value="${oauthForm.password}" placeholder="密码" test-id="password" required minlength="6">
								</div>
								<div class="ct-input" message-for="password" test-type="required">密码不能为空</div>
								<div class="ct-input" message-for="password" test-type="minlength">请输入6位以上密码</div>
								<label><input type="checkbox" name="remeber"><a>记住我</a></label>
								<br>
								<input type="button" class="btn-login" value="登  录">
								<input type="button" class="btn-registered" value="注  册">
								<div class="ct-input error-message">
								<c:if test="${not empty error}">
									${error}
								</c:if>
								<c:if test="${error eq ''}">
									服务器忙，请稍候重试
								</c:if>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>	
	</div>
	
	<!-- 尾部 -->
	<div class="login-footer">
		<div class="footer">			
		    主管单位：上海电力学院　　主办单位：
	    	<a href="" target="_blank">上海电力学院研究生调剂系统</a>
	    	<a href="" style="color:#C00;" target="_blank">沪ICP证030485号</a>
	    	<br>
    		承办单位：上海电力学院
		</div>
	</div>
	<form id="pannelSubmit" action="/" method="get"></form>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/dist/js/SimValidate.js"></script>
	<script type="text/javascript">
		$(function(){
			var loginValidate = $('#loginValidate').SimValidate({messageStyle:'color:red;padding-left:3px;padding-right:6px;width:auto;display:block;top:-15px;'});
			$('.btn-login').click(function(){
				if(loginValidate.validate){
					$('#loginForm').submit();
				}
			});
			$('.btn-registered').click(function(){
				window.location.href='${ctx}'+'/toRegist';
			});
		});
	</script>
</body>
</html>