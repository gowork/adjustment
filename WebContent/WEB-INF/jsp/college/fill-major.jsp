<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="func" uri="http://java.sun.com/jsp/jstl/functions" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>学生报考情况</title>
	<link rel="stylesheet" href="dist/css/major.css">
	<style>
		.direct {
		    text-align: left;
		    padding-left: 30px;
		}
		.editable{
			outline:none;
		}
	</style>
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div style="clear:both;"></div>
	<div class="body-content">
		<div class="search">
			<h2>查看及添加专业方向</h2>
		</div>
		<div class="major-list">
			<table class="first">
				<thead>
					<tr>
						<th style="width:33.33333%">学院</th>
						<th style="width:33.33333%">专业 <a href="javascript:;" data-id="${majorCollection.collegeId}" class="addBtn addMajorBtn">添加专业</a></th>
						<th style="width:33.33333%">方向</th>
					</tr>
				</thead>
				<tbody id="majorList">
					<c:if test="${func:length(majorCollection.directCollectList)>0}">
					<tr>
						<td data-id="${majorCollection.collegeId}" class="first college" style="width:33.33333%">${majorCollection.collegeName}</td>
						<td colspan="2" class="first">
						<table class="second major-tab">
						<c:forEach var="major" varStatus="status" items="${majorCollection.directCollectList}">
							<tr class="major-item">
								<td class="second" style="width:50%">
									<div class="">
										<div class="major editable" data-major-id="${major.majorId}" data-major-name="${major.majorName}">${major.majorName}</div>
									</div>
								</td>
								<td class="second" style="width:50%">
									<a href="javascript:;" data-major-id="${major.majorId}" class="addBtn addDirectBtn">添加专业方向</a>
									<c:if test="${func:length(major.directList)>0}">
									<c:forEach var="direct" items="${major.directList}">
									<div class=" direct">
										<div class="editable" data-id="${direct.id}" data-name="${direct.name}">${direct.name}</div>
									</div>
									</c:forEach>
									</c:if>
								</td>
							</tr>
						</c:forEach>
						</table>
					</tr>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script type="text/javascript" src="/dist/js/jquery.livequery.js"></script>
	<script type="text/javascript">
	$(function(){
		$('.editable').livequery('focusin',function(){
			var input = $(this).text();
			if(input==$(this).data('placeholder')){
				$(this).text('');
			}
		}).livequery('focusout',function(){
			var input = $(this).text();
			if(!input){
				$(this).html($(this).data('placeholder'));
			}
		});

		$('.addMajorBtn').livequery('click',function(){
			var collegeId = $(this).data('id');
			var addModal = dialog({
				title:'添加专业',
				content:'<div class="editor_item"><div class="editable" contenteditable="true" id="addMajorInput" data-placeholder="请输入专业">请输入专业</div></div>',
				okValue: '添加',
			    ok: function () {
			    	//访问数据库添加  添加成功后关闭该窗口查询数据
			    	var majorName = $('#addMajorInput').text();
			    	var majorForm = {};
			    	majorForm['collegeId'] = collegeId;
			    	majorForm['majorName'] = majorName;
			    	if(majorName&&majorName!=$('#addMajorInput').data('placeholder')){
						$.ajax({
			                cache: true,
			                type: 'POST',
			                url:'college/addmajor',
			                dataType:"json",      
			                contentType:"application/json",               
			                data:JSON.stringify(majorForm),
			                async: false,
			                success: function(data) {
			                	var status= data.status;
			                	if(status==1){
			                		showMajors();
			                	}
			                }
						});

			    	}
			    }
			}).height(30).width(300).show();
		});
		$('.addDirectBtn').livequery('click',function(){
			var majorId = $(this).data('major-id');
			var addModal = dialog({
				title:'添加专业方向',
				content:'<div class="editor_item"><div class="editable" id="addDirectInput" contenteditable="true" data-placeholder="请输入专业方向">请输入专业方向</div></div>',
				okValue: '添加',
			    ok: function () {
			    	//访问数据库添加  添加成功后关闭该窗口查询数据
			    	var name = $('#addDirectInput').text();
			    	var directForm = {};
			    	directForm['majorId'] = majorId;
			    	directForm['name'] = name;
			    	if(name && name!=$('#addDirectInput').data('placeholder')){
						$.ajax({
			                cache: true,
			                type: 'POST',
			                url:'college/adddirect',
			                dataType:"json",      
			                contentType:"application/json",               
			                data:JSON.stringify(directForm),
			                async: false,
			                success: function(data) {
			                	var status= data.status;
			                	if(status==1){
			                		showMajors();
			                	}
			                }
						});

			    	}
			    }
			}).height(30).width(300).show();
		});
		
	});
	
	function showMajors(){
		$.ajax({
            cache: true,
            type: 'POST',
            url:'college/showallmajor',
            data:{},
            async: false,
            success: function(majorView) {
            	var status= majorView.status;
            	if(status==1){
            		var majorCollection = majorView.value;
            		var majorHtml = [];
            		if(majorCollection && majorCollection.directCollectList && majorCollection.directCollectList.length>0){
            			$.each(majorCollection.directCollectList,function(majorIndex,major){
                    		majorHtml.push('<tr class="major-item">');
                    		majorHtml.push('	<td class="second" style="width:50%">');
                    		majorHtml.push('		<div class="">');
                    		majorHtml.push('			<div class="major editable" data-major-id="'+(major.majorId?major.majorId:'')+'">'+(major.majorName?major.majorName:'')+'</div>');
                    		majorHtml.push('		</div>');
                    		majorHtml.push('	</td>');
                    		majorHtml.push('	<td class="second" style="width:50%">');
                    		majorHtml.push('		<a href="javascript:;" data-major-id="'+(major.majorId?major.majorId:'')+'" class="addBtn addDirectBtn">添加专业方向</a>');
                    		if(major.directList&& major.directList.length>0){
                    			$.each(major.directList,function(directIndex,direct){
                            		majorHtml.push('		<div class="direct">');
                            		majorHtml.push('			<div class="editable">'+(direct.name?direct.name:'')+'</div>');
                            		majorHtml.push('		</div>');
                    			});
                    		}
                    		majorHtml.push('	</td>');
                    		majorHtml.push('</tr>');
            			});
            		}
            		$('.major-tab').html(majorHtml.join(''));
            	}
            }
		});		
	}
	</script>
</body>
</html>