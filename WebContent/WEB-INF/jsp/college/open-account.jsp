<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>开立帐号</title>
	<link rel="stylesheet" href="dist/css/openAccount.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container" id="openAccountContent">
			<div class="container-right">
				<h2 class="post-title">开立帐号</h2>
				<div class="form-info">
					<div class="form-row">
						<div class="form-title">帐号类型</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<select class="form-ipt" name="accountType">
								<option value="1">可开新账号</option>
								<option value="2" selected>不可开新账号</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">姓名</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="name" value="" class="form-ipt" test-id="name" required>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="name" test-type="required">请输入姓名</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">用户名</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" autocomplete="off" name="username" value="" class="form-ipt" test-id="username" required minlength="3">
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="username" test-type="required">请输入用户名</span>
							<span class="error-message" message-for="username" test-type="minlength">请输入3个字符以上用户名</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">密码</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="password" autocomplete="off" name="password" value="" class="form-ipt" test-id="password" required minlength="6">
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="newPassword" test-type="required">请输入密码</span>
							<span class="error-message" message-for="newPassword" test-type="minlength">请输入6-15位密码</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox form-tips">
							<span class="error-message js-error">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<button class="btn btn-primary">保  存</button>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script src="dist/js/SimValidate.js"></script>
	<script src="dist/js/openAccount.js"></script>
</body>
</html>