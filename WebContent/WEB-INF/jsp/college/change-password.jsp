<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>修改密码</title>
	<link rel="stylesheet" href="dist/css/openAccount.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container">
			<div class="container-right">
				<form id="changePasswordForm" action="changepassword" method="post">
				<h2 class="post-title">修改密码</h2>
				<div class="form-info">
					<div class="form-row">
						<div class="form-title">原始密码</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="password" name="oldPassword" value="${passwordForm.oldPassword}" class="form-ipt" test-id="oldPassword" required minlength="6">
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="oldPassword" test-type="required">请输入原始密码</span>
							<span class="error-message" message-for="oldPassword" test-type="minlength">请输入6-15位原始密码</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">新密码</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="password" name="newPassword" value="${passwordForm.newPassword}" class="form-ipt" test-id="newPassword" required minlength="6">
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="newPassword" test-type="required">请输入新密码</span>
							<span class="error-message" message-for="newPassword" test-type="minlength">请输入6-15位新密码</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">确认密码</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="password" name="confirmPassword" value="${passwordForm.confirmPassword}" class="form-ipt"  test-id="confirmPassword" required minlength="6" equalTo="newPassword">
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="confirmPassword" test-type="required">请输入确认密码</span>
							<span class="error-message" message-for="confirmPassword" test-type="minlength">请输入6-15位确认密码</span>
							<span class="error-message" message-for="confirmPassword" test-type="equalTo">两次输入密码不一致</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">${error}</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox form-tips">
							<button class="btn btn-primary">保  存</button>
						</div>
					</div>
				</div>
				</form>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script src="dist/js/SimValidate.js"></script>
	<script src="dist/js/changePassword.js"></script>
</body>
</html>