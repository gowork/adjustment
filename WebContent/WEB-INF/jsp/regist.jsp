<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>帐号注册-上海电力学院预调剂系统</title>
	<link rel="stylesheet" href="dist/css/regist.css">
	<script type="text/javascript" src="${ctx}/dist/js/jquery2.1.4.min.js"></script>
</head>
<body>
	<div class="content">
		<div class="header">
			<div class="header-link">
			</div>
			<div class="header-txt">
				<a href="http://www.shiep.edu.cn/" target="_blank">上海电力学院</a>
				　|　
				<a href="http://yjsc.web.shiep.edu.cn/" target="_blank">上海电力学院研究生招生</a>
			</div>
		</div>
		<!-- 注册盒子 -->
		<div class="box clearfix">
			<div class="t">
				欢迎注册预调剂系统帐号
				<div class="tlogin">
				已有帐号<a href="/toLogin" class="regs-login">立即登录</a>
				</div>
			</div>
			<div id="registValidate" class="main-box clearfix">
				<form id="registForm" action="/regist" autocomplete="off" method="post">
					<div class="main-txt">
						注册成功后可以使用上海电大学院调剂系统提供的服务。（
						<a href="" target="_blank" class="colorblue" style="text-decoration:underline;">什么是预调剂系统帐号？</a>
						<a href="" target="_blank" class="colorblue" style="text-decoration:underline;">了解更多</a>
						）
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">手机号</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="text" name="tel" test-id="tel" value="${registForm.tel}" required mobile class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="tel" test-type="required">手机号不能为空</span>
							<span class="error-message" message-for="tel" test-type="mobile">请输入正确的手机号</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">准考证号</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="text" name="ticketNumber" test-id="ticketNumber" value="${registForm.ticketNumber}" required class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="ticketNumber" test-type="required">准考证号不能为空</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">身份证号</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="text" name="idcard" test-id="idcard" value="${registForm.idcard}" required idcard class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="idcard" test-type="required">身份证号不能为空</span>
							<span class="error-message" message-for="idcard" test-type="idcard">请输入正确的身份证号</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">邮箱</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="text" name="email" test-id="email" value="${registForm.email}" required email class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="email" test-type="required">邮箱不能为空</span>
							<span class="error-message" message-for="email" test-type="email">请输入正确的邮箱</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">密码</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="password" name="password" test-id="password" value="${registForm.password}" required minlength="6" maxlength="15" class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="password" test-type="required">密码不能为空</span>
							<span class="error-message" message-for="password" test-type="minlength">请输入6-15位数字、字母、特殊字符组成的密码</span>
							<span class="error-message" message-for="password" test-type="maxlength">请输入6-15位数字、字母、特殊字符组成的密码</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">确认密码</div>
						<div class="regs-impt">*</div>
						<div class="regs-iptbox">
							<input type="password" test-id="confirmpassword" value="${registForm.password}" required equalTo="password" class="regs-ipt">
						</div>
						<div class="regs-iptbox mleft10 regs-tips">
							<span class="error-message" message-for="confirmpassword" test-type="required">确认密码不能为空</span>
							<span class="error-message" message-for="confirmpassword" test-type="equalTo">两次输入的密码不一致</span>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">&nbsp;</div>
						<div class="regs-iptbox">
							<input type="checkbox" value="1" checked="checked">
							我已阅读并同意
							<a href="" target="_blank" class="colorblue">服务条款</a>
						</div>
					</div>
					<div class="regs-line clearfix">
						<div class="regs-title">&nbsp;</div>
						<div class="regs-iptbox">
							<input type="button" value="立即注册" class="regs-submit">&nbsp;&nbsp;&nbsp;
							<span class="error-message">
							<c:if test="${not empty error}">
								${error}
							</c:if>
							<c:if test="${error eq ''}">
								服务器忙，请稍候重试
							</c:if>
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/dist/js/SimValidate.js"></script>
	<script type="text/javascript">
	var registValidate = $('#registValidate').SimValidate();
	$('.regs-submit').click(function(){
		if(registValidate.validate()){
			$('#registForm').submit();
		}
	});
	</script>
</body>
</html>