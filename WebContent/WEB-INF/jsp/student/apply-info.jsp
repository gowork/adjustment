<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>拟调剂信息</title>
	<link rel="stylesheet" href="dist/css/apply-info.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container">
			<div class="container-left">
				<nav class="list-group">
					<a href="student/baseinfo" class="list-group-item">基本信息</a>
					<a href="student/examinfo" class="list-group-item">报考学校及成绩</a>
					<a href="student/applyinfo" class="list-group-item active">拟调剂信息</a>
					<a href="tochangepassword" class="list-group-item">修改密码</a>
				</nav>
			</div>
			<div class="container-center">&nbsp;</div>
			<div class="container-right">
				<h2 class="post-title">拟调剂信息</h2>
				<form id="applyInfoForm" method="post">
				<div class="form-info">
					<div class="form-row">
						<div class="form-title">第一志愿</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<select id="targetCollegeId1" class="form-ipt" data-value="${applyInfoView.targetList[0].targetCollegeId}" name="targetFormList[0].targetCollegeId">
								<option value="">--请选择学院--</option>
								<c:forEach var="college" items="${collegeList}">
									<option value="${college.id}" ${applyInfoView.targetList[0].targetCollegeId eq college.id?'selected':''}>${college.name}</option>
								</c:forEach>
							</select>
							<select id="targetMajorId1" class="form-ipt" data-value="${applyInfoView.targetList[0].targetMajorId}" name="targetFormList[0].targetMajorId">
								<option value="">--请选择专业--</option>
							</select>
							<select id="targetDirectionId1" class="form-ipt" data-value="${applyInfoView.targetList[0].targetDirectionId}" name="targetFormList[0].targetDirectionId">
								<option value="">--请选择方向--</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">第二志愿</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<select id="targetCollegeId2" class="form-ipt" data-value="${applyInfoView.targetList[1].targetCollegeId}" name="targetFormList[1].targetCollegeId">
								<option value="">--请选择学院--</option>
								<c:forEach var="college" items="${collegeList}">
									<option value="${college.id}" ${applyInfoView.targetList[1].targetCollegeId eq college.id?'selected':''}>${college.name}</option>
								</c:forEach>
							</select>
							<select id="targetMajorId2" class="form-ipt" data-value="${applyInfoView.targetList[1].targetMajorId}" name="targetFormList[1].targetMajorId">
								<option value="">--请选择专业--</option>
							</select>
							<select id="targetDirectionId2" class="form-ipt" data-value="${applyInfoView.targetList[1].targetDirectionId}" name="targetFormList[1].targetDirectionId">
								<option value="">--请选择方向--</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">是否接受校内调</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox">
							<label class="radio-inline"><input name="isAcceptSwap" type="radio" value="1" ${applyInfoView.isAcceptSwap eq '1'?'checked':''}> 是</label>
							<label class="radio-inline"><input name="isAcceptSwap" type="radio" value="2" ${applyInfoView.isAcceptSwap eq '2'?'checked':''}> 否</label>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<button type="button" class="btn btn-primary">保  存</button>
					</div>
				</div>
				</form>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script src="dist/js/SimValidate.js"></script>
	<script src="dist/js/studentApplyInfo.js"></script>
</body>
</html>