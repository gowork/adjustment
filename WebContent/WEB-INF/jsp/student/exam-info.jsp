<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>报考学校及成绩</title>
	<link rel="stylesheet" href="dist/css/exam-info.css">
	<style type="text/css">
		.form-tips {
		    width: 465px;
		    margin-left: 120px;
		    color: #666;
		}
		.sub-title{
		    font-size: 16px;
		    font-weight: 400;
		    color: #0773B5;
		}
		.form-row .input-space{
			margin-left:5px;
		}
		.form-row .tip{
			background-color: #dff0ff;
		    color: #3b83c0;
		    width: 524px;
		}
	</style>
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container">
			<div class="container-left">
				<nav class="list-group">
					<a href="student/baseinfo" class="list-group-item">基本信息</a>
					<a href="student/examinfo" class="list-group-item active">报考学校及成绩</a>
					<a href="student/applyinfo" class="list-group-item">拟调剂信息</a>
					<a href="tochangepassword" class="list-group-item">修改密码</a>
				</nav>
			</div>
			<div class="container-center">&nbsp;</div>
			<div id="examInfoContent" class="container-right">
				<h2 class="post-title">研究生考试报考学校及成绩</h2>
				<div class="form-info">
					<div class="form-row">
						<div class="form-title sub-title">报考情况</div>
					</div>
					<div class="form-row">
						<div class="form-title">报考学校</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="postSchoolName" value="${examInfoView.postSchoolName}" class="form-ipt" test-id="postSchoolName" required>
						</div>
						<div class="form-title">报考专业</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="postSchoolMajor" value="${examInfoView.postSchoolMajor}" class="form-ipt" test-id="postSchoolMajor" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title sub-title">考研成绩</div>
						<div class="form-iptbox tip">
								（注：工程管理只需填写其他方面）						
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">数学</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<select class="form-ipt" name="mathName" style="width:200px;">
								<option value="数一" ${(examInfoView.mathName eq '数一')?'selected':''}>数一</option>
								<option value="数二" ${(examInfoView.mathName eq '数二')?'selected':''}>数二</option>
								<option value="数三" ${(examInfoView.mathName eq '数三')?'selected':''}>数三</option>
							</select>
						</div>
						<div class="form-impt input-space">&nbsp;</div>
						<div class="form-iptbox">
							<input type="text" name="mathScore" value="${examInfoView.mathScore}" class="form-ipt" test-id="mathScore" required placeholder="请输入数学成绩">
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">外语</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<select class="form-ipt" name="englishName" style="width:200px;">
								<option value="英语一" ${(examInfoView.englishName eq '英语一')?'selected':''}>英语一</option>
								<option value="英语二" ${(examInfoView.englishName eq '英语二')?'selected':''}>英语二</option>
								<option value="其他" ${(examInfoView.englishName eq '其他')?'selected':''}>其他</option>
							</select>
						</div>
						<div class="form-impt input-space">&nbsp;</div>
						<div class="form-iptbox">
							<input type="text" name="englishScore" value="${examInfoView.englishScore}" class="form-ipt" placeholder="请输入外语成绩">
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">政治</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="polityScore" value="${examInfoView.polityScore}" class="form-ipt" test-id="polityScore" required placeholder="请输入政治成绩">
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">专业课1</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<input type="text" name="business2Name" value="${examInfoView.business1Name}" class="form-ipt" placeholder="请输入专业课1名称">
						</div>
						<div class="form-impt input-space">&nbsp;</div>
						<div class="form-iptbox">
							<input type="text" name="business1Score" value="${examInfoView.business1Score}" class="form-ipt" placeholder="请输入专业课1成绩">
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">专业课2</div>
						<div class="form-impt">&nbsp;</div>
						<div class="form-iptbox">
							<input type="text" name="business2Name" value="${examInfoView.business2Name}" class="form-ipt" placeholder="请输入专业课2名称">
						</div>
						<div class="form-impt input-space">&nbsp;</div>
						<div class="form-iptbox">
							<input type="text" name="business2Score" value="${examInfoView.business2Score}" class="form-ipt" placeholder="请输入专业课2成绩">
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">总分</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="totalScore" value="${examInfoView.totalScore}" class="form-ipt" test-id="totalScore" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="postSchoolName" test-type="required">请输入报考学校</span>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="postSchoolMajor" test-type="required">请输入报考专业</span>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="polityScore" test-type="required">请输入政治成绩</span>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="totalScore" test-type="required">请输入总分</span>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message result-error"></span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<button class="btn btn-primary">保  存</button>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script src="dist/js/SimValidate.js"></script>
	<script src="dist/js/studentExamInfo.js"></script>
</body>
</html>