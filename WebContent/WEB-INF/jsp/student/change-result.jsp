<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>修改结果</title>
	<link rel="stylesheet" href="dist/css/base-info.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container">
			<div class="container-left">
				<nav class="list-group">
					<a href="student/baseinfo" class="list-group-item">基本信息</a>
					<a href="student/examinfo" class="list-group-item">报考学校及成绩</a>
					<a href="student/applyinfo" class="list-group-item">拟调剂信息</a>
					<a href="tochangepassword" class="list-group-item active">修改密码</a>
				</nav>
			</div>
			<div class="container-center">&nbsp;</div>
			<div class="container-right">
				<h2 class="post-title">修改结果</h2>
				<div class="form-info">
					<div class="form-row">
						<div class="form-title">修改成功</div>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
</body>
</html>