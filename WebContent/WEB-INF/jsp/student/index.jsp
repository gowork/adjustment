<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>学生主页</title>
	<link rel="stylesheet" href="dist/css/student-index.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="body-content">
		<div class="content-row">
			<div class="content-main-benefit">
				<div class="content-main-benefit-pannel" data-href="student/baseinfo">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-applyinfo.png" alt=""></div>
						<h2>基本信息</h2>
						<p>填写个人基本信息</p>
					</div>
				</div>
				<div class="content-main-benefit-pannel" data-href="student/examinfo">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-account.png" alt=""></div>
						<h2>报考学校及成绩</h2>
						<p>填写已经报考的学校及考研各科成绩</p>
					</div>
				</div>
				<div class="content-main-benefit-pannel" data-href="student/applyinfo">
					<div>
						<div class="content-main-benefit-pic"><img src="dist/img/icons/btn-major.png" alt=""></div>
						<h2>拟调剂信息</h2>
						<p>填写拟调剂的专业及个人信息等</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<form id="pannelSubmit" action="/" method="post"></form>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.content-main-benefit-pannel').click(function(){
				var href = $(this).data('href');
				$('#pannelSubmit').attr('action',href);
				$('#pannelSubmit').submit();
			});
		});
	</script>
</body>
</html>