<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>基本信息</title>
	<link rel="stylesheet" href="dist/css/base-info.css">
</head>
<body>
	<%@include file="/dist/include/header.jsp"%>
	<div class="wrap">
		<div class="container">
			<div class="container-left">
				<nav class="list-group">
					<a href="student/baseinfo" class="list-group-item active">基本信息</a>
					<a href="student/examinfo" class="list-group-item">报考学校及成绩</a>
					<a href="student/applyinfo" class="list-group-item">拟调剂信息</a>
					<a href="tochangepassword" class="list-group-item">修改密码</a>
				</nav>
			</div>
			<div class="container-center">&nbsp;</div>
			<div class="container-right" id="baseInfoContent">
				<h2 class="post-title">基本信息</h2>
				<div class="form-info">
					<div class="form-row">
						<div class="form-title">姓名</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="name" value="${baseInfo.name}" class="form-ipt" test-id="name" required>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="name" test-type="required">请输入姓名</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">性别</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<select name="sex" class="form-ipt">
								<option value="男" ${baseInfo.sex eq '男'?'selected':''}>男</option>
								<option value="女" ${baseInfo.sex eq '女'?'selected':''}>女</option>
							</select>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">紧急联系电话</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="emergContactTel" value="${baseInfo.emergContactTel}" class="form-ipt" test-id="emergContactTel" required>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="emergContactTel" test-type="required">请输入紧急联系电话</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">本科学校</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="undergraduateSchool" value="${baseInfo.undergraduateSchool}" class="form-ipt" test-id="undergraduateSchool" required>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="undergraduateSchool" test-type="required">请输入本科学校</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">本科专业</div>
						<div class="form-impt">*</div>
						<div class="form-iptbox">
							<input type="text" name="undergraduateMajor" value="${baseInfo.undergraduateMajor}" class="form-ipt" test-id="undergraduateMajor" required>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message" message-for="undergraduateMajor" test-type="required">请输入本科专业</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">自我评价</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<textarea name="evaluation" class="form-textarea" rows="5">${baseInfo.evaluation}</textarea>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">特长</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<textarea name="special" class="form-textarea" rows="3">${baseInfo.special}</textarea>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">获奖情况</div>
						<div class="form-impt"></div>
						<div class="form-iptbox">
							<textarea name="prize" class="form-textarea" rows="6">${baseInfo.prize}</textarea>
						</div>
						<div class="form-iptbox form-tips">
							<span class="error-message">&nbsp;</span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-title">&nbsp;</div>
						<div class="form-impt">&nbsp;</div>
						<button class="btn btn-primary">保  存</button>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<%@include file="/dist/include/footer.jsp"%>
	<script src="dist/js/SimValidate.js"></script>
	<script src="dist/js/studentBaseInfo.js"></script>
</body>
</html>