<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
String path = request.getContextPath();  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
%>
<c:set var="ctx" value="<%=basePath%>"/>
<!DOCTYPE html>
<html>
<head>
	<base href="${ctx}">
	<meta charset="utf-8">
	<title>no oauth_您没有权限访问该页面_上海电力学院预调剂系统</title>
	<link rel="stylesheet" href="dist/css/login.css">
	<link rel="stylesheet" href="dist/css/error.css">
</head>
<body>
	<div class="login-header">
		<div class="login-container clearfix">
			<div class="login-header-grid grid clearfix alpha">
		      	<h1>上海电力大学研究生调剂系统</h1>
		      	<div class="login-logo-grid grid">
		      		<span class="logo_chsi logo_chsi_yz">&nbsp;</span>
		      	</div>
	    	</div>
			<div class="login-topright-grid grid clearfix">
				<div class="top-right">
					<a href="http://www.shiep.edu.cn/" target="_blank">上海电力学院</a>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="http://yjsc.web.shiep.edu.cn/" target="_blank">上海电力学院研究生处</a>
				</div>
			</div>
		</div>
	</div>

	<div class="nofound-top clearfix">
		<div class="nofound-number">
			<div class="nofound-floor">
				<div class="electric"></div>
				<div class="floor-gif">
					<img src="dist/img/background/floor.gif" width="27" height="115">
				</div>
			</div>
		</div>
	</div>
	<div class="nofound-bottom clearfix">
		<div class="nofound-msg">
			<p class="nofound-word">你没有权限进行此操作，请返回  <a href="/">首页</a></p>
		</div>
	</div>
	
	<div class="login-footer">
		<div class="footer">			
		    主管单位：上海电力学院　　主办单位：
	    	<a href="" target="_blank">上海电力学院研究生调剂系统</a>
	    	<a href="" style="color:#C00;" target="_blank">沪ICP证030485号</a>
	    	<br>
    		承办单位：上海电力学院
		</div>
	</div>
<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	//灯泡闪动
    setTimeout(show, 500);

   	//显示
    function show() {
       $('.electric').addClass('electric-open')
       setTimeout(hide, 500);
    }
	//隐藏
    function hide() {
        $('.electric').removeClass('electric-open')
        setTimeout(show, 500);
    }
});
</script>
</body>
</html>